import {combineReducers} from 'redux';
import promises from './promises';
import appStateReducer from './appStateReducer';
import userReducer from './user';
import consultation from '../../containers/consult-online/consult-online.reducer';
import {reducer as formReducer} from 'redux-form';

export default combineReducers({
  appStateReducer,
  user: userReducer,
  promises,
  form: formReducer,
  consultation,
});
