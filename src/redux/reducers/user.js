import Toast from 'react-native-simple-toast';
import ip, {chain} from 'icepick';
import {
  GET_PROFILES_LIST_SUCCESS,
  GET_USER_DATA_SUCCESS,
} from '../../containers/home-screen/home-screen.actions.const';
import {LOGIN_SUCCESSFUL, LOGIN_FAILED} from '../constants';
import idx from 'idx';
import {LOGOUT_USER, VERIFY_OTP_SUCCESS} from '../actions/user.actions.const';

import {setAuthHeaders} from 'api';
import {SAVE_PATIENT_DATA_SUCCESS} from '../../containers/edit-profile/edit-profile.actions.const';

const initialState = {
  userData: null,
  profiles: [],
};
export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESSFUL:
      return action.payload.data;

    case LOGOUT_USER:
      return initialState;

    case LOGIN_FAILED:
      Toast.show('Invalid username or password,please try again');
      return state;

    case GET_USER_DATA_SUCCESS:
    case VERIFY_OTP_SUCCESS: {
      let accessToken = idx(action, (_) => _.payload.accessToken) || '';
      if (accessToken) {
        setAuthHeaders({accessToken});
      }
      return chain(state)
        .setIn(['userData'], idx(action, (_) => _.payload) || [])
        .value();
    }

    case GET_PROFILES_LIST_SUCCESS:
      return chain(state)
        .setIn(['profiles'], idx(action, (_) => _.payload) || [])
        .value();

    case SAVE_PATIENT_DATA_SUCCESS: {
      let profiles = ip.push(state.profiles, action.payload);
      return chain(state).setIn(['profiles'], profiles).value();
    }

    default:
      return state;
  }
};
