import ip, {chain} from 'icepick';
import {LOGOUT_USER} from '../actions/user.actions.const';
const BASE_URL_LOCAL = 'http://api.jigglemart.com';

const initialState = ip.freeze({
  apiNetworkError: false,
  loginData: null,
  config: {
    baseURL: BASE_URL_LOCAL,
    cloudBaseURL: '',
    headers: '',
    accessToken: '',
  },
  remoteConfig: {
    networkWaitTimeInSeconds: 30,
    smsCodeExpiryTimerInSeconds: 30,
    smsCodeRetriesLimit: 6,
    smsCodeLength: 6,
    testUserEnable: 0,
    phoneAuthEnable: 'false',
  },
});

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case 'LOGIN_TO_SERVER_SUCCESS':
      return chain(state)
        .assocIn(['user', 'key'], payload.key)
        .assocIn(['user', 'userId'], payload.user)
        .value();

    case LOGOUT_USER:
      return initialState;

    // case LOGIN_SUCCESSFUL:
    //   return chain(state)
    //     .setIn(['loginData'], payload.data)
    //     .setIn(['config', 'accessToken'], idx(payload, _ => _.data.accessToken))
    //     .value();

    default:
      return state;
  }
};
