const HOME_BG = require('./home_bg.png');
const CHAT_WITH_DOCTOR = require('./chat_with_doctor.png');
const HOSPITAL = require('./hospital.png');
const MEDICINE = require('./medicine.png');
const DENT = require('./dent.png');
const SPC_COVID = require('./spc-covid.png');
const SPC_HEART = require('./spc-heart.png');
const SPC_BRAIN = require('./spc-brain.png');
const LEFT = require('./leftArrow.png');
const SEARCH = require('./search.png');
const DOCTOR_IMAGE_PLACEHOLDER = require('./doctorsPic.png');
const BAG_BLUE = require('./bagBlue.png');
const STAR_FILLED = require('./starFilled.png');
const HOME_INACTIVE = require('./homeinactive.png');
const ACCOUNT_INACTIVE = require('./accountInactive.png');
const CONSULT_INACTIVE = require('./consultInactive.png');
const RECORDS_INACTIVE = require('./recordsInactive.png');
const RECORDS_PROCEDURES = require('./icProfileProcedures.png');
const ICON_NEXT = require('./icNext.png');
const ACCOUNT = require('./icAccountNormal.png');
const HELP = require('./icHelpWhite.png');
const ADDITIONAL_INFO = require('./iconTitle.png');
const ADD_ICON = require('./iconAdd.png');
const SEARCH_ICON_SECOND = require('./searchIconSecond.png');
const SYMPTOMS = require('./symptoms.png');
const SELECT_ITEM = require('./selectItem.png');
const SELECTED_ITEM = require('./selectedItem.png');
const ADD_PATIENT = require('./icAddPatient.png');
const SELECT_PATIENT = require('./icSelectPatient.png');
const SYMPTOM_PLACE_HOLDER = require('./symptom_place_holder.png');
const ICON_CLOSE = require('./icClose.png');
const ILLUS_WELCOME = require('./welcome.png');
const APP_LOGO = require('./iconLogoDochub.png');
const ONBOARDING_INTRO = require('./onboardingintro.png');
const LOCATION_PIN = require('./marker1.png');
const CREDIT_CARD = require('./creditCard.png');
const ICON_REPORT = require('./interface.png');
const ICON_MEMBERSHIP = require('./sportsAndCompetition.png');
const CUSTOMER_SUPPORT = require('./customerSupport.png');
const ICON_SETTINGS = require('./settings.png');
const ICON_LOGOUT = require('./logout.png');
const FEMALE_BODY = require('./humanBody.png');
const ICON_HEART = require('./heart.png');
const ICON_OXYGEN = require('./o2level.png');
const ICON_TEMPERATURE = require('./temperature.png');
const ICON_EDIT = require('./editIcon.png');
const ICON_CALENDAR = require('./calendar.png');
const ICON_DROPDOWN = require('./dropDown.png');
const ANTI_BIOTIC = require('./antibiotic.png');
const NOTIFICATIONS = require('./notifications.png');
export {
  HOME_BG,
  CHAT_WITH_DOCTOR,
  HOSPITAL,
  MEDICINE,
  DENT,
  SPC_BRAIN,
  SPC_HEART,
  SPC_COVID,
  LEFT,
  SEARCH,
  DOCTOR_IMAGE_PLACEHOLDER,
  BAG_BLUE,
  STAR_FILLED,
  HOME_INACTIVE,
  ACCOUNT_INACTIVE,
  CONSULT_INACTIVE,
  RECORDS_INACTIVE,
  RECORDS_PROCEDURES,
  ICON_NEXT,
  ACCOUNT,
  HELP,
  ADDITIONAL_INFO,
  ADD_ICON,
  SEARCH_ICON_SECOND,
  SYMPTOMS,
  SELECTED_ITEM,
  SELECT_ITEM,
  ADD_PATIENT,
  SELECT_PATIENT,
  SYMPTOM_PLACE_HOLDER,
  ICON_CLOSE,
  ILLUS_WELCOME,
  APP_LOGO,
  ONBOARDING_INTRO,
  LOCATION_PIN,
  CREDIT_CARD,
  ICON_MEMBERSHIP,
  CUSTOMER_SUPPORT,
  ICON_SETTINGS,
  ICON_LOGOUT,
  ICON_REPORT,
  FEMALE_BODY,
  ICON_HEART,
  ICON_OXYGEN,
  ICON_TEMPERATURE,
  ICON_EDIT,
  ICON_CALENDAR,
  ICON_DROPDOWN,
  ANTI_BIOTIC,
  NOTIFICATIONS,
};
