import React from 'react';
import {useEffect, useRef} from 'react';
import {FlatList, KeyboardAvoidingView, Platform, Keyboard} from 'react-native';
import BottomInputBox from '../components/molecules/bottom-input-box';
import ChatHeader from '../components/molecules/chat-header';
import ChatMessageItem from '../components/molecules/chat-message-item';

const TEST_MESSAGES = [
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Test',
  'Last Message',
];

const MESSAGES = [
  {
    sender: 'source',
  },
  {
    sender: 'source',
  },
  {
    sender: 'source',
  },
  {
    sender: 'target',
  },
  {
    sender: 'source',
  },
  {
    sender: 'taget',
  },
  {
    sender: 'source',
  },
  {
    sender: 'target',
  },
  {
    sender: 'target',
  },
  {
    sender: 'source',
  },
  {
    sender: 'taget',
  },
  {
    sender: 'source',
  },
  {
    sender: 'source',
  },
  {
    sender: 'target',
  },
  {
    sender: 'source',
  },
  {
    sender: 'taget',
  },
];

export default function ChatScreen() {
  const list = useRef();

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
    };
  }, []);

  const _keyboardDidShow = () => {
    list.current.scrollToOffset(100);
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{flex: 1}}>
      <ChatHeader />
      <FlatList
        contentContainerStyle={{paddingHorizontal: 10}}
        inverted={-1}
        ref={list}
        data={MESSAGES.reverse()}
        renderItem={({item}) => <ChatMessageItem {...item} />}
      />
      <BottomInputBox />
    </KeyboardAvoidingView>
  );
}
