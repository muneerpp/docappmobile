import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import Title from '../components/atoms/title';

export default function UIelements() {
  return (
    <ScrollView>
      <View style={styles.wrapper}>
        <Title text="Online Shopping" />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingHorizontal: 10,
    ...ifIphoneX({paddingTop: 45}, {paddingTop: 20}),
  },
});
