/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  Image,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import {SEARCH} from 'images';
import Row from '../components/atoms/row';
import DoctorCard from '../components/molecules/doctor-card';
import HeaderNavigation from '../components/molecules/header-navigation';
import colors from '../config/colors';

const SearchBox = () => (
  <View style={styles.searchBoxWrapper}>
    <Image source={SEARCH} style={styles.searchIcon} />
    <TextInput style={styles.input} placeholder="Search For" />
  </View>
);

const FilterChip = ({isSelected, label}) => (
  <View
    style={[
      styles.filterChipWrapper,
      {
        borderWidth: isSelected ? 1 : 0,
        backgroundColor: isSelected
          ? 'rgba(0, 115, 250, 0.1)'
          : 'rgba(0, 0, 0, 0.08)',
      },
    ]}>
    <Text
      style={[
        styles.filterChipText,
        {
          color: isSelected ? '#0073fa' : colors.blackTwo,
          opacity: isSelected ? 1 : 0.6,
          fontWeight: isSelected ? 'bold' : 'normal',
        },
      ]}>
      {label}
    </Text>
  </View>
);

const FilterChips = () => (
  <View
    style={{
      flexDirection: 'row',
      marginHorizontal: 10,
    }}>
    <FilterChip isSelected label="All Doctors" />
    <FilterChip label="Anesthesiology" />
    <FilterChip label="Cardiologists" />
    <FilterChip label="Allergy" />
  </View>
);

const ActionButton = ({label, onPress}) => (
  <TouchableOpacity style={styles.actionButtonWrapper} onPress={onPress}>
    <Text style={styles.actionButtonText}>{label}</Text>
  </TouchableOpacity>
);

const ActionButtons = ({navigation}) => (
  <Row customStyles={styles.actionButtonsWrapper}>
    <View style={{flex: 1}} />
    <ActionButton label="Share" />
    <ActionButton label="Know More" />
    <ActionButton
      label="Consult"
      onPress={() => navigation.navigate('ChatScreen')}
    />
  </Row>
);

export default function ChooseDoctor({navigation}) {
  return (
    <View style={styles.wrapper}>
      <View style={styles.searchContainer}>
        <HeaderNavigation />
        <SearchBox />
        <FilterChips />
      </View>
      <Text style={styles.resultLabel}>Showing Available Doctors Now</Text>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={Array(10).fill('')}
        renderItem={() => (
          <View style={styles.doctorCardSection}>
            <DoctorCard />
            <ActionButtons navigation={navigation} />
          </View>
        )}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: '#f4f4f4',
  },
  searchBoxWrapper: {
    flexDirection: 'row',
    height: 46,
    borderRadius: 12,
    backgroundColor: '#f4f4f4',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#dedede',
    marginHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  searchIcon: {
    width: 20,
    height: 20,
  },
  input: {
    flex: 1,
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1b1b1b',
    paddingLeft: 10,
  },
  searchContainer: {
    backgroundColor: 'white',
    paddingBottom: 10,
  },
  resultLabel: {
    height: 25,
    fontSize: 14,
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
    paddingHorizontal: 10,
    marginTop: 15,
    marginBottom: 10,
  },
  filterChipWrapper: {
    height: 32,
    borderRadius: 16,

    borderStyle: 'solid',

    borderColor: '#0073fa',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    marginRight: 10,
    marginTop: 15,
  },
  filterChipText: {
    height: 16,
    fontSize: 12,

    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
  },
  actionButtonsWrapper: {
    // padding: 10,
    paddingVertical: 10,
    paddingRight: 5,
    backgroundColor: 'white',
    marginHorizontal: 10,
    borderBottomLeftRadius: 12,
    borderBottomRightRadius: 12,
    justifyContent: 'space-between',
  },
  actionButtonWrapper: {
    alignItems: 'flex-end',
    height: 32,
    borderRadius: 16,
    backgroundColor: 'rgb( 0 ,115, 250)',
    paddingHorizontal: 20,
    justifyContent: 'center',
    marginLeft: 10,
  },
  actionButtonText: {
    fontSize: 14,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.white,
  },
  doctorCardSection: {
    backgroundColor: '#f6f6f6',
    marginBottom: 10,
  },
});
