import React from 'react';
import {View, Text, StyleSheet, Image, ScrollView} from 'react-native';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {DOCTOR_IMAGE_PLACEHOLDER, ICON_NEXT, RECORDS_PROCEDURES} from 'images';
import IconComponent from '../components/icon-component';
import colors from '../config/colors';

const MENU_ITEMS = [
  {
    text: 'Basic Information',
    image: RECORDS_PROCEDURES,
  },
  {
    text: 'Prescriptions',
    image: RECORDS_PROCEDURES,
  },
  {
    text: 'Attachments and reports',
    image: RECORDS_PROCEDURES,
  },
  {
    text: 'Reminders',
    image: RECORDS_PROCEDURES,
  },
  {
    text: 'Health Metrics',
    image: RECORDS_PROCEDURES,
  },
  {
    text: 'Allergies',
    image: RECORDS_PROCEDURES,
  },
  {
    text: 'Immunization',
    image: RECORDS_PROCEDURES,
  },
  {
    text: 'Medications',
    image: RECORDS_PROCEDURES,
  },
  {
    text: 'Procedures',
    image: RECORDS_PROCEDURES,
  },
  {
    text: 'Subscriptions',
    image: RECORDS_PROCEDURES,
  },
];

const UserSelector = () => (
  <View style={styles.userSelectorWrapper}>
    <Image source={DOCTOR_IMAGE_PLACEHOLDER} style={styles.userImage} />
    <Text style={styles.userName}>Nora Shelton</Text>
  </View>
);

export default function RecordsHome() {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>My Records</Text>
      <ScrollView showsVerticalScrollIndicator={false}>
        <UserSelector />
        <View style={styles.menuItemsWrapper}>
          {MENU_ITEMS.map((item) => (
            <View style={styles.itemWrapper}>
              <IconComponent icon={item.image} size={40} />
              <View style={styles.itemText}>
                <Text style={styles.item}>{item.text}</Text>
              </View>
              <IconComponent icon={ICON_NEXT} size={16} />
            </View>
          ))}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: 'rgba(244,244,244,0.01)',
    paddingHorizontal: 10,
    ...ifIphoneX({paddingTop: 50}, {paddingTop: 30}),
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 28,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
  },
  userSelectorWrapper: {
    alignItems: 'center',
    paddingVertical: 30,
  },
  userImage: {
    width: 112,
    height: 112,
    borderRadius: 32,
  },
  userName: {
    fontSize: 17,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
    marginVertical: 15,
  },
  menuItemsWrapper: {
    borderRadius: 16,
    backgroundColor: colors.white,
    shadowColor: 'rgba(0, 64, 128, 0.04)',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowRadius: 10,
    shadowOpacity: 1,
  },
  itemWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: '#fafafa',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 15,
    paddingHorizontal: 10,
  },
  itemText: {
    flex: 1,
    paddingLeft: 15,
  },
  item: {
    fontSize: 15,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 18,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
  },
});
