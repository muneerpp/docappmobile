import React from 'react';
import {View, Text} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {} from '@react-navigation/compat';
import StoreDetails from '../containers/consumer/store-details/store-details.container';
import CategoryHome from '../containers/consumer/category-home/category-home.container';
import CartScreen from '../containers/consumer/cart-screen/cart.container';
import Checkout from '../containers/consumer/checkout';

const Stack = createStackNavigator();

const StoreDetailsStack = () => (
  <Stack.Navigator screenOptions={{headerShown: false}}>
    <Stack.Screen name="StoreDetails" component={StoreDetails} />
    <Stack.Screen name="CategoryHome" component={CategoryHome} />
    <Stack.Screen name="Cart" component={CartScreen} />
  </Stack.Navigator>
);

export default StoreDetailsStack;
