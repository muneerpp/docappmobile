const colors = {
  paleGrey: '#e5ebfc',
  paleGreyTwo: '#ecf2ff',
  white: '#ffffff',
  veryLightPink: '#ffebeb',
  paleSalmon: '#f1aeae',
  brownishPink: '#d97474',
  pastelRed: '#ec4e4e',
  vibrantGreen: '#08c514',
  black: '#202020',
  blue: '#0042e0',
  azul: '#1a60fd',
  blackTwo: '#000000',
  topaz: '#12b2b3',
  brightCyan: '#56e0e0',
  azure: '#1da1f2',
  blueyGrey: '#9393aa',
  whiteTwo: '#fafafa',
};

export default colors;
