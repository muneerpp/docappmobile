import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import colors from '../../config/colors';

export default function Button({label, onPress, color, height}) {
  return (
    <TouchableOpacity
      style={[styles.wrapper, {backgroundColor: color, height: height || 55}]}
      onPress={onPress}>
      <Text style={styles.label}>{label}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.azul,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  label: {
    fontSize: 17,
    fontWeight: '600',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.white,
  },
});
