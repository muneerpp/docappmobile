import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import {titleCase} from '../../utils/helpers';
import Row from '../atoms/row';
import IconComponent from '../icon-component';

const PickerInput = ({icon, value, placeholder, onPress, disableTitlecase}) => (
  <Row customStyles={styles.wrapper}>
    <TouchableOpacity style={styles.inputWrapper} onPress={onPress}>
      <Text
        // eslint-disable-next-line react-native/no-inline-styles
        style={{
          opacity: value ? 1 : 0.3,
        }}>
        {disableTitlecase
          ? value || placeholder
          : titleCase(value) || titleCase(placeholder)}
      </Text>
    </TouchableOpacity>
    <IconComponent
      icon={icon}
      size={20}
      outerStyle={styles.iconWrapper}
      onPress={onPress}
    />
  </Row>
);

export default PickerInput;

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
  },
  inputWrapper: {
    height: 46,
    borderRadius: 8,
    backgroundColor: 'rgba(255, 255, 255, 0)',
    borderStyle: 'solid',
    borderWidth: 2,
    borderColor: '#0073fa',
    justifyContent: 'center',
    paddingLeft: 10,
    flex: 1,
  },
  iconWrapper: {
    position: 'absolute',
    right: 0,
    paddingHorizontal: 10,
  },
});
