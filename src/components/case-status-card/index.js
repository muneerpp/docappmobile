import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import colors from '../../config/colors';
import Title from '../atoms/title';
import {DOCTOR_IMAGE_PLACEHOLDER} from 'images';
import {titleCase} from '../../utils/helpers';
import idx from 'idx';

const Consultation = ({status, name, id, careTeam, onPress}) => {
  let teamName = idx(careTeam, (_) => _.name);
  return (
    <TouchableOpacity onPress={onPress} style={styles.wrapper}>
      <View style={styles.imageSectionWrapper}>
        <Image source={DOCTOR_IMAGE_PLACEHOLDER} style={styles.image} />
      </View>
      <View style={styles.dataWrapper}>
        <Text style={styles.caseName}>{name}</Text>
        <Text style={styles.caseName}>Case ID : {id}</Text>
        <Text style={styles.doctorName}>{teamName}</Text>
        <View style={styles.chatBox}>
          <Text style={styles.chatText}>Which Language you prefer ?</Text>
        </View>
      </View>

      <View style={styles.caseStatusWrapper}>
        <Text style={styles.caseStatusText}>{titleCase(status)}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default function CaseStatusCard({cases = [], onSelectCase}) {
  return (
    <View>
      <Title text="My Consultations" customStyle={styles.title} />
      <ScrollView
        horizontal
        pagingEnabled={true}
        showsHorizontalScrollIndicator={false}>
        {cases.length > 0
          ? cases.map((caseItem) => (
              <Consultation
                {...caseItem}
                status="Pending"
                onPress={() => onSelectCase(caseItem)}
              />
            ))
          : null}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    textAlign: 'left',
    paddingBottom: 10,
  },
  wrapper: {
    borderRadius: 16,
    backgroundColor: colors.white,
    shadowColor: 'rgba(0, 64, 128, 0.10)',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowRadius: 10,
    shadowOpacity: 1,
    marginBottom: 40,
    marginTop: 20,
    flexDirection: 'row',
    marginRight: 10,
  },
  imageSectionWrapper: {
    marginHorizontal: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 60,
    height: 60,
    borderRadius: 6,
  },
  dataWrapper: {
    flex: 1,
    paddingVertical: 10,
  },
  caseName: {
    fontSize: 13,
    fontWeight: '400',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blueyGrey,
    marginVertical: 5,
  },
  doctorName: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 18,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
    marginVertical: 5,
  },
  chatBox: {
    backgroundColor: colors.paleGrey,
    padding: 10,
    marginRight: 10,
    borderRadius: 8,
    marginVertical: 5,
  },
  chatText: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
  },
  caseStatusWrapper: {
    position: 'absolute',
    right: 10,
    top: 10,
    backgroundColor: colors.brownishPink,
    padding: 5,
    borderRadius: 4,
    paddingHorizontal: 10,
  },
  caseStatusText: {
    fontSize: 12,
    fontWeight: '400',
    color: colors.white,
  },
});
