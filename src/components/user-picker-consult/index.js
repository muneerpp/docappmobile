import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity,
} from 'react-native';
import {ADD_PATIENT, SELECT_PATIENT} from 'images';
import colors from '../../config/colors';
import IconComponent from '../icon-component';
import Image from 'react-native-fast-image';

const PatientCard = ({id, relation, name, selectedUserId, onPress, image}) => (
  <TouchableOpacity style={styles.patCardWrapper} onPress={() => onPress(id)}>
    {image ? (
      <Image
        source={{uri: image}}
        style={[
          id === selectedUserId
            ? styles.activeIconWrapper
            : styles.inactiveIconWrapper,
        ]}
      />
    ) : (
      <View
        style={[
          id === selectedUserId
            ? styles.activeIconWrapper
            : styles.inactiveIconWrapper,
        ]}>
        <IconComponent
          icon={SELECT_PATIENT}
          size={30}
          onPress={() => onPress(id)}
          color={id === selectedUserId ? colors.white : colors.blueyGrey}
        />
      </View>
    )}
    <View style={styles.patNameWrapper}>
      <Text style={styles.patName}>
        {name} {relation ? `(${relation})` : ''}
      </Text>
    </View>
  </TouchableOpacity>
);

const AddPatientCard = ({onPress}) => (
  <TouchableOpacity style={styles.patCardWrapper} onPress={onPress}>
    <View style={styles.inactiveIconWrapper}>
      <IconComponent icon={ADD_PATIENT} size={30} color={colors.blueyGrey} />
    </View>
    <View style={styles.patNameWrapper}>
      <Text style={styles.patName}>Add</Text>
    </View>
  </TouchableOpacity>
);

export default function UserPickerConsult({
  profiles,
  onPress,
  navigation,
  activeProfile,
}) {
  return (
    <View style={styles.wrapper}>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
        {profiles.map((pat) => (
          <PatientCard
            {...pat}
            selectedUserId={(activeProfile || {}).id}
            onPress={() => onPress(pat)}
          />
        ))}
        <AddPatientCard
          onPress={() => navigation.navigate('EditProfile', {edit: false})}
        />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  activeIconWrapper: {
    width: 72,
    height: 72,
    backgroundColor: colors.azure,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: colors.azure,
  },
  inactiveIconWrapper: {
    width: 72,
    height: 72,
    borderRadius: 16,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#e0e0e0',
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: {
    paddingHorizontal: 32,
    paddingVertical: 24,
  },
  patName: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 22,
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.blueyGrey,
  },
  patNameWrapper: {
    paddingVertical: 10,
  },
  patCardWrapper: {
    marginHorizontal: 10,
    alignItems: 'center',
  },
});
