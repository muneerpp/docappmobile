/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import ImagePicker from 'react-native-image-crop-picker';

const PHOTO = 'PHOTO';
const VIDEO = 'VIDEO';
// const CLOUD = 'CLOUD';
const GALLERY = 'GALLERY';

export default function CameraPickerOptions({onSelectMedia, options}) {
  const onPickerItemPressed = (id) => {
    switch (id) {
      case PHOTO:
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          cropping: true,
        }).then((image) => {
          onSelectMedia(image);
        });
        break;

      case VIDEO:
        ImagePicker.openCamera({
          mediaType: 'video',
        }).then((image) => {
          onSelectMedia(image);
        });
        break;

      case GALLERY:
        ImagePicker.openPicker({
          mediaType: 'photo',
        }).then((video) => {
          onSelectMedia(video);
        });
        break;

      default:
        break;
    }
  };

  return (
    <View>
      {options.map((option, index) => (
        <TouchableOpacity
          onPress={() => onPickerItemPressed(option.id)}
          style={[
            styles.item,
            {
              borderBottomWidth: options.length !== index + 1 ? 1 : 0,
            },
          ]}>
          <Text style={styles.text}>{option.text}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  item: {
    paddingVertical: 15,
    paddingHorizontal: 10,
    borderBottomColor: '#f6f6f6',
  },
  text: {
    fontSize: 15,
    fontStyle: 'normal',
    lineHeight: 18,
    letterSpacing: 0,
    color: '#1e1f20',
  },
});
