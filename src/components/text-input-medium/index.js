import React from 'react';
import {View, TextInput} from 'react-native';

export default function TextInputMedium({ref}) {
  return (
    <TextInput
      multiline
      ref={ref}
      style={{
        height: 128,
        borderRadius: 8,
        backgroundColor: '#fafafa',
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#f0f0f0',
      }}
    />
  );
}
