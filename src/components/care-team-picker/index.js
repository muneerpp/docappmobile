import React from 'react';
import {View, ScrollView, StyleSheet, Text, Dimensions} from 'react-native';
import colors from '../../config/colors';
import DoctorCard from '../molecules/doctor-card';

const {width} = Dimensions.get('window');

export default function CareTeamPicker({
  careTeams = [],
  onSelectCareTeam,
  careTeamId,
}) {
  return (
    <View style={styles.wrapper}>
      <ScrollView horizontal>
        {careTeams.length > 0 ? (
          careTeams.map((team) => (
            <View
              style={[
                careTeamId && careTeamId === team.id
                  ? styles.selectedItem
                  : styles.cardWrapper,
              ]}>
              <DoctorCard
                onPress={() => {
                  onSelectCareTeam(team);
                }}
                {...team}
              />
            </View>
          ))
        ) : (
          <View style={styles.emptyTextWrapper}>
            <Text style={styles.emptyText}>
              Sorry No Care Team Is available at the moment
            </Text>
          </View>
        )}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  selectedItem: {
    marginRight: 10,
    padding: 10,
    backgroundColor: colors.white,
    height: 180,
    borderRadius: 6,
    borderColor: colors.azure,
    borderWidth: 2,
    shadowColor: colors.azure,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 6,
    elevation: 5,
  },
  cardWrapper: {
    marginRight: 10,
    padding: 10,
    backgroundColor: colors.white,
    height: 180,
    borderRadius: 6,
  },
  wrapper: {
    height: 250,
    width: '100%',
    padding: 20,
    backgroundColor: colors.paleGrey,
  },
  emptyTextWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    width: width * 0.9,
  },
  emptyText: {
    textAlign: 'center',
    fontSize: 16,
    maxWidth: '60%',
    lineHeight: 26,
  },
});
