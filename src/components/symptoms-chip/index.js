import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import {SELECTED_ITEM, SELECT_ITEM} from 'images';
import IconComponent from '../icon-component';

const SymptomChip = ({name, isSelected, width, onPress}) => (
  <TouchableOpacity
    style={[styles.wrapper, {width: width || '50%'}]}
    onPress={onPress}>
    <Text style={styles.text}>{name}</Text>
    <IconComponent
      icon={isSelected ? SELECTED_ITEM : SELECT_ITEM}
      size={30}
      onPress={onPress}
    />
  </TouchableOpacity>
);

export default SymptomChip;

const styles = StyleSheet.create({
  wrapper: {
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(1, 1, 1, 0.05)',
    borderTopWidth: 1,
    borderTopColor: 'rgba(1, 1, 1, 0),0.05',
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  text: {
    fontSize: 15,
    fontWeight: '600',
    fontStyle: 'normal',
    lineHeight: 24,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1da1f2',
    flex: 1,
  },
});
