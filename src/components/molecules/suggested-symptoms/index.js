import React from 'react';
import {View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import {SYMPTOMS} from 'images';
import {SYMPTOMS_STUB} from '../../../config/stubs';
import SymptomChip from '../../symptoms-chip';
import CardWrapper from '../card-wrapper';

export default function SuggestedSymptoms() {
  return (
    <ScrollView>
      <CardWrapper title="Commonly Selected Symptoms" icon={SYMPTOMS}>
        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          {SYMPTOMS_STUB.map((symptom) => (
            <SymptomChip {...symptom} isSelected={false} />
          ))}
        </View>
      </CardWrapper>
    </ScrollView>
  );
}
