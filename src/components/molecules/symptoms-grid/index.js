import {withNavigation} from '@react-navigation/compat';
import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {DENT} from 'images';
import colors from '../../../config/colors';
const SYMPTOMS_STUB = [
  {
    title: 'COVID-19',
    image: DENT,
  },
  {
    title: 'COVID-19',
    image: DENT,
  },
  {
    title: 'COVID-19',
    image: DENT,
  },
  {
    title: 'COVID-19',
    image: DENT,
  },
  {
    title: 'COVID-19',
    image: DENT,
  },
  {
    title: 'COVID-19',
    image: DENT,
  },
];

function SymptomsGrid({navigation}) {
  return (
    <View>
      <Text style={styles.label}>Common Symptoms</Text>
      <View style={styles.sectionWrapper}>
        {SYMPTOMS_STUB.map((symptom) => (
          <TouchableOpacity onPress={() => navigation.navigate('ChooseDoctor')}>
            <View style={styles.gridItem}>
              <Image source={symptom.image} style={styles.symptomImage} />
            </View>
            <Text style={styles.title}>{symptom.title}</Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
}

export default withNavigation(SymptomsGrid);

const styles = StyleSheet.create({
  label: {
    fontSize: 18,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
  },
  sectionWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginVertical: 20,
  },
  gridItem: {
    width: 101,
    height: 98,
    borderRadius: 8,
    backgroundColor: colors.white,
    shadowColor: 'rgba(236, 236, 236, 0.5)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 6,
    shadowOpacity: 1,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#f5f5f5',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
  },
  symptomImage: {
    width: 45,
    height: 45,
  },
  title: {
    height: 30,
    fontSize: 13,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: 'rgba(0, 0, 0, 0.6)',
    marginTop: 10,
  },
});
