import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import colors from '../../../config/colors';
import IconComponent from '../../icon-component';

const CardWrapper = ({
  title,
  children,
  icon,
  RightComponent,
  onRightPressed,
}) => (
  <View style={styles.cardContainer}>
    <View style={styles.cartTitleWrapper}>
      <IconComponent icon={icon} size={24} outerStyle={styles.cardIconOuter} />
      <Text style={styles.cartTitleText}>{title}</Text>
      {RightComponent ? <RightComponent /> : null}
    </View>
    {children}
  </View>
);

const styles = StyleSheet.create({
  cardContainer: {
    minHeight: 223,
    borderRadius: 16,
    backgroundColor: colors.white,
  },
  cartTitleWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 16,
    paddingHorizontal: 16,
    borderBottomWidth: 1,
    borderBottomColor: '#f6f6f6',
  },
  cardIconOuter: {
    width: 32,
    height: 32,
    backgroundColor: 'rgba(18,178,179,0.16)',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
  },
  cartTitleText: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 18,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
    marginLeft: 16,
    flex: 1,
  },
});

export default CardWrapper;
