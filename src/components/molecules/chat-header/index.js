import {withNavigation} from '@react-navigation/compat';
import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {DOCTOR_IMAGE_PLACEHOLDER, LEFT} from 'images';
import colors from '../../../config/colors';
import IconComponent from '../../icon-component';

function ChatHeader({navigation}) {
  return (
    <View style={styles.wrapper}>
      <IconComponent
        icon={LEFT}
        size={20}
        onPress={() => navigation.goBack()}
      />
      <Image source={DOCTOR_IMAGE_PLACEHOLDER} style={styles.thumbNail} />
      <View style={styles.contentWrapper}>
        <Text style={styles.name}>Dr. Alvert Lewinston</Text>
        <Text style={styles.designation}>Physician</Text>
        <Text style={styles.organisation}>Aster Hospital</Text>
      </View>
    </View>
  );
}

export default withNavigation(ChatHeader);

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.white,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: {
      width: 0,
      height: 18,
    },
    shadowRadius: 40,
    shadowOpacity: 1,
    paddingBottom: 25,
    flexDirection: 'row',
    ...ifIphoneX({paddingTop: 50}, {paddingTop: 30}),
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  thumbNail: {
    width: 48,
    height: 48,
    borderRadius: 48 / 2,
    marginHorizontal: 20,
  },
  contentWrapper: {
    flex: 1,
  },
  name: {
    opacity: 0.9,
    fontSize: 14,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
  },
  designation: {
    opacity: 0.9,
    fontSize: 10,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
    paddingVertical: 5,
  },
  organisation: {
    opacity: 0.9,
    fontSize: 10,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
  },
});
