import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import colors from '../../../config/colors';

export default function BottomInputBox() {
  return (
    <View style={styles.wrapper}>
      <TextInput
        underlineColorAndroid="rgba(1,1,1,0)"
        style={{}}
        multiline={false}
        placeholder="type something..."
        // onChangeText={(text) => this.updateText(text)}
        // value={this.state.text}
        placeholderTextColor="rgba(1,1,1,0.5)"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    opacity: 0.9,
    backgroundColor: colors.white,
    paddingHorizontal: 10,
    paddingVertical: 10,
    ...ifIphoneX({paddingBottom: 30}),
  },
});
