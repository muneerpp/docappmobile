import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {DOCTOR_IMAGE_PLACEHOLDER, STAR_FILLED} from 'images';
import colors from '../../../config/colors';
import Row from '../../atoms/row';

export default function DoctorCard({onPress, name, description}) {
  return (
    <TouchableOpacity style={styles.wrapper} onPress={onPress}>
      <Image source={DOCTOR_IMAGE_PLACEHOLDER} style={styles.doctorImage} />
      <View style={styles.contentWrapper}>
        <Text style={styles.name}>{name}</Text>
        <Text style={styles.speciality}>{description}</Text>
        <Row>
          <Row>
            <Image style={styles.icon} source={STAR_FILLED} />
            <Text style={styles.experienceText}>5 Years</Text>
          </Row>
          <Row>
            <Image style={styles.icon} source={STAR_FILLED} />
            <Text style={styles.experienceText}>4.5 (123)</Text>
          </Row>
        </Row>
        <Row>
          <Text style={styles.languagesLabel}>Speaks :</Text>
          <Text style={styles.languages}>Hindi,English,Tamil</Text>
        </Row>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    height: 120,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
    backgroundColor: colors.white,
    marginHorizontal: 10,
    flexDirection: 'row',
  },
  name: {
    fontSize: 12,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
  },
  speciality: {
    opacity: 0.6,
    fontSize: 10,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
  },
  experienceText: {
    fontSize: 8,
    fontWeight: 'normal',
    fontStyle: 'normal',
    textAlign: 'left',
    color: colors.blackTwo,
    marginLeft: 5,
    marginRight: 10,
  },
  doctorImage: {
    width: 100,
    height: 120,
    borderRadius: 12,
  },
  contentWrapper: {
    flex: 1,
    paddingLeft: 15,
    paddingVertical: 20,
    justifyContent: 'space-between',
  },
  languagesLabel: {
    fontSize: 12,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
    marginRight: 5,
  },
  languages: {
    fontSize: 12,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
  },
  icon: {
    width: 11.4,
    height: 12,
  },
});
