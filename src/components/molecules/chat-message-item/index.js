/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import colors from '../../../config/colors';

export default function ChatMessageItem({sender}) {
  return (
    <View
      style={[
        styles.wrapper,
        {
          backgroundColor: sender === 'target' ? colors.white : '#0073fa',
          alignSelf: sender === 'target' ? 'flex-start' : 'flex-end',
        },
      ]}>
      <Text
        style={[
          styles.time,
          {
            opacity: sender === 'target' ? 0.6 : 1,
            color: sender === 'target' ? colors.blackTwo : colors.white,
          },
        ]}>
        05.00 PM
      </Text>
      <Text
        style={[
          styles.message,
          {
            color: sender === 'target' ? colors.blackTwo : colors.white,
          },
        ]}>
        Hi Kitani, how about your pregnancy condition? do you feel pain?
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    maxWidth: '60%',
    minHeight: 60,
    borderRadius: 16,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: {
      width: 0,
      height: 18,
    },
    shadowRadius: 40,
    shadowOpacity: 1,
    marginBottom: 10,
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  time: {
    fontSize: 10,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
  },
  message: {
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',

    lineHeight: 18,
    paddingVertical: 10,
  },
});
