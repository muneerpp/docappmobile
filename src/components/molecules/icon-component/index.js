import React from 'react';
import {TouchableOpacity, Image} from 'react-native';

export default function IconComponent({onPress, color, size, icon}) {
  return (
    <TouchableOpacity onPress={onPress}>
      <Image
        source={icon}
        style={{
          width: size,
          height: size,
        }}
      />
    </TouchableOpacity>
  );
}
