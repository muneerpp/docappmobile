import React from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';
import {PRIMARY_FONT} from '../../../assets/fonts';
import {CAMERA_OUTLINE} from 'images';
import colors from '../../../config/colors';
import commonStyles from '../../../utils/comonStyles';
import IconComponent from '../../atoms/icon';

const SearchBox = () => (
  <View style={styles.searchBoxWrapper}>
    <TextInput
      placeholder="Search"
      placeholderTextColor={colors.blue}
      style={styles.searchBox}
    />
    <View style={styles.searchIconWrapper}>
      <IconComponent icon={CAMERA_OUTLINE} />
    </View>
  </View>
);

export default function SearchBar() {
  return (
    <View style={commonStyles.row}>
      <Text style={styles.title}>Shop</Text>
      <SearchBox />
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    fontFamily: PRIMARY_FONT,
    fontSize: 28,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: -0.28,
    textAlign: 'left',
    color: colors.black,
  },
  searchBoxWrapper: {
    height: 36,
    borderRadius: 18,
    flex: 1,
    marginLeft: 20,
    flexDirection: 'row',
  },
  searchBox: {
    height: 36,
    borderRadius: 18,
    flex: 1,
    flexDirection: 'row',
    backgroundColor: 'rgba(0,66,224,0.1)',
    paddingHorizontal: 10,
    fontFamily: PRIMARY_FONT,
    fontSize: 16,
    fontWeight: '500',
    fontStyle: 'normal',
    lineHeight: 21,
    letterSpacing: -0.16,
    textAlign: 'left',
  },
  searchIconWrapper: {
    alignSelf: 'flex-end',
    right: 10,
    position: 'absolute',
  },
});
