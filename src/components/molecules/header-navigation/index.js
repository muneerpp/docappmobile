import {withNavigation} from '@react-navigation/compat';
import React from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {LEFT} from 'images';
import colors from '../../../config/colors';
import IconComponent from '../../icon-component';

function HeaderNavigation({navigation, title, RightComponent}) {
  return (
    <View style={styles.wrapper}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <IconComponent
          icon={LEFT}
          size={20}
          onPress={() => navigation.goBack()}
        />
      </TouchableOpacity>

      <View style={styles.titleWrapper}>
        <Text style={styles.title}>{title}</Text>
      </View>
      {RightComponent && <RightComponent />}
    </View>
  );
}

export default withNavigation(HeaderNavigation);

const styles = StyleSheet.create({
  wrapper: {
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: {
      width: 0,
      height: 18,
    },
    shadowRadius: 40,
    shadowOpacity: 1,
    paddingBottom: 25,
    flexDirection: 'row',
    alignItems: 'center',
    ...ifIphoneX({paddingTop: 50}, {paddingTop: 30}),
    paddingHorizontal: 10,
    backgroundColor: colors.white,
  },
  leftIcon: {
    width: 18,
    height: 18,
  },
  titleWrapper: {
    flex: 1,
    paddingLeft: '5%',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
  },
});
