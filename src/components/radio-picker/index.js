import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import colors from '../../config/colors';
import {titleCase} from '../../utils/helpers';

export default function RadioPicker({onPress, selectedId, items}) {
  return (
    <View style={styles.wrapper}>
      {items.map((rel) => (
        <TouchableOpacity
          onPress={() => onPress(rel)}
          style={[
            styles.chipWrapper,
            {
              backgroundColor:
                selectedId === rel.id ? colors.azure : colors.veryLightPink,
            },
          ]}>
          <Text
            style={[
              styles.chipText,
              {
                color: selectedId === rel.id ? colors.white : colors.blueyGrey,
              },
            ]}>
            {titleCase(rel.value)}
          </Text>
        </TouchableOpacity>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    width: widthPercentageToDP(90),
    alignSelf: 'center',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: heightPercentageToDP(10),
  },
  chipWrapper: {
    width: widthPercentageToDP(90 / 3) - widthPercentageToDP(2),
    justifyContent: 'space-between',
    marginVertical: 10,
    padding: 10,
    borderRadius: 12,
    alignItems: 'center',
  },
  chipText: {
    fontSize: 17,
    fontWeight: '600',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
  },
});
