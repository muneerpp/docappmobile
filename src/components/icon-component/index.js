import React from 'react';
import {TouchableOpacity, Image} from 'react-native';

export default function IconComponent({
  icon,
  color,
  size,
  onPress,
  outerStyle,
}) {
  return (
    <TouchableOpacity onPress={onPress} style={outerStyle}>
      <Image
        source={icon}
        style={{
          width: size,
          height: size,
          tintColor: color,
          resizeMode: 'contain',
        }}
      />
    </TouchableOpacity>
  );
}
