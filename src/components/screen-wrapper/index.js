import React from 'react';
import {View, StyleSheet} from 'react-native';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default function ScreenWrapper({children, backgroundColor}) {
  return <View style={[styles.wrapper, {backgroundColor}]}>{children}</View>;
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    ...ifIphoneX(
      {
        paddingTop: 50,
      },
      {
        paddingTop: 30,
      },
    ),
  },
});
