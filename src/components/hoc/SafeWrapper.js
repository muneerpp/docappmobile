import React from 'react';
import {View, StyleSheet} from 'react-native';
import {ifIphoneX} from 'react-native-iphone-x-helper';

export default function SafeWrapper({children, customStyle}) {
  return <View style={[styles.wrapper, customStyle]}>{children}</View>;
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingHorizontal: 10,
    ...ifIphoneX({paddingTop: 55}, {paddingTop: 35}),
  },
});
