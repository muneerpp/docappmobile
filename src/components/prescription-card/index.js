/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {
  ANTI_BIOTIC,
  ICON_EDIT,
  ICON_DELETE,
  ICON_APPROVE,
} from '../../assets/images';
import colors from '../../config/colors';
import {titleCase} from '../../utils/helpers';
import IconComponent from '../icon-component';

const MedicineMeta = ({label, value}) => (
  <View style={styles.medicineDataWrapper}>
    <Text style={styles.label}>{label}</Text>
    <Text style={styles.value}>: {value}</Text>
  </View>
);

export default function PrescriptionCard({
  medicine = {},
  onDelete,
  onApprove,
  metaData,
  onEdit,
}) {
  return (
    <View style={styles.wrapper}>
      <Image source={ANTI_BIOTIC} style={styles.diagImage} />

      <View style={styles.dataWrapper}>
        <Text style={styles.diagName}>{titleCase(medicine.name)}</Text>
        <View
          style={{
            marginTop: 10,
          }}>
          {Object.keys(metaData.usageInfo).map((item) => (
            <MedicineMeta
              label={titleCase(item)}
              value={titleCase(
                `${metaData.usageInfo[item].value} ${metaData.usageInfo[item].unit}`,
              )}
            />
          ))}
        </View>
      </View>

      <View style={styles.diagActionsWrapper}>
        {onApprove ? (
          <IconComponent
            icon={ICON_APPROVE}
            size={16}
            color={colors.white}
            outerStyle={styles.iconOuter}
            onPress={onApprove}
          />
        ) : null}

        {onDelete ? (
          <IconComponent
            icon={ICON_DELETE}
            size={20}
            color={colors.white}
            outerStyle={styles.iconOuter}
            onPress={onDelete}
          />
        ) : null}

        {onEdit ? (
          <IconComponent
            icon={ICON_EDIT}
            size={20}
            color={colors.white}
            outerStyle={[styles.iconOuter, {backgroundColor: colors.azul}]}
          />
        ) : null}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    borderRadius: 16,
    backgroundColor: colors.veryLightPink,
    shadowColor: 'rgba(0, 64, 128, 0.04)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 2,
    shadowOpacity: 1,
    marginHorizontal: 5,
    marginVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 6,
    paddingVertical: 10,
  },
  diagImage: {
    width: 100,
    height: 100,
    borderRadius: 8,
  },
  dataWrapper: {
    marginLeft: 12,
    flex: 1,
  },
  diagName: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 18,
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.topaz,
  },
  diagDescription: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
    marginTop: 12,
  },
  diagActionsWrapper: {
    width: 40,
    height: 100,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  medicineDataWrapper: {
    flexDirection: 'row',
    maxWidth: '80%',
    marginBottom: 2,
  },
  label: {
    fontSize: 10,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 18,
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blueyGrey,
    flex: 1,
  },
  value: {
    fontSize: 10,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 18,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
    flex: 1,
  },
  iconOuter: {
    width: 24,
    height: 24,
    backgroundColor: colors.lightGrey,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
  },
});
