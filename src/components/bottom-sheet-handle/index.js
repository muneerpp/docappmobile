import React from 'react';
import {View, Text, StyleSheet, Platform} from 'react-native';
import colors from '../../config/colors';

const BottomSheetHandle = () => {
  return (
    <View style={styles.rootContainer}>
      <View style={styles.handleIndicator} />
    </View>
  );
};

const styles = StyleSheet.create({
  rootContainer: {
    backgroundColor: colors.white,
    flexDirection: 'row',
    justifyContent: 'center',
    borderTopLeftRadius: 10,
    borderLeftColor: colors.lightGrey,
    borderLeftWidth: 1,
    borderTopRightRadius: 10,
    borderRightColor: colors.lightGrey,
    borderRightWidth: 1,
    borderTopColor: colors.paleGrey,
    borderTopWidth: 1,
    top: -5,
  },

  handleIndicator: {
    width: 50,
    height: 5,
    backgroundColor: colors.lightGrey,
    borderRadius: 10,
    marginTop: 7,
  },
});

export default BottomSheetHandle;
