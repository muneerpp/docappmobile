import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import colors from '../../config/colors';

export default function InputBox({prefix, ...rest}) {
  return (
    <View style={styles.containerWrapper}>
      {prefix ? (
        <View style={styles.prefixWrapper}>
          <Text style={styles.prefixText}>+91</Text>
        </View>
      ) : null}
      <TextInput
        style={[
          styles.input,
          // eslint-disable-next-line react-native/no-inline-styles
          {
            paddingLeft: prefix ? widthPercentageToDP(10) : 10,
          },
        ]}
        {...rest}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  containerWrapper: {
    flexDirection: 'row',
  },
  prefixWrapper: {
    position: 'absolute',
    height: 46,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: widthPercentageToDP(1.2),
  },
  prefixText: {
    opacity: 0.35,
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
  },
  input: {
    height: 46,
    borderRadius: 8,
    backgroundColor: 'rgba(255, 255, 255, 0)',
    borderStyle: 'solid',
    borderWidth: 2,
    borderColor: '#0073fa',
    flex: 1,
  },
});
