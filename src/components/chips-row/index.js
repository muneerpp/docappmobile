import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import colors from '../../config/colors';

export default function ChipsRow({perRowCount, count}) {
  let balance = count % perRowCount ? perRowCount - (count % perRowCount) : 0;
  return (
    <View style={styles.wrapper}>
      {Array(count)
        .fill('')
        .map((item) => (
          <View style={styles.chip}>
            <Text numberOfLines={1} style={styles.text}>
              Peanut Allergey
            </Text>
          </View>
        ))}

      {Array(balance)
        .fill('')
        .map((item) => (
          <View style={[styles.chip, {backgroundColor: 'red'}]}>
            <Text numberOfLines={1} style={styles.text} />
          </View>
        ))}
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  chip: {
    width: widthPercentageToDP(100 / 3.5),
    marginBottom: 10,
    height: 29,
    backgroundColor: '#4a5cd0',
    borderRadius: 6,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.white,
    maxWidth: widthPercentageToDP(100 / 4) - 10,
  },
});
