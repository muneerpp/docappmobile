import React from 'react';
import {View, StyleSheet} from 'react-native';

export default function Row({children, customStyles}) {
  return <View style={[styles.wrapper, customStyles]}>{children}</View>;
}

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
