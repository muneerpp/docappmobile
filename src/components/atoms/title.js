import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {PRIMARY_FONT} from '../../assets/fonts';
import colors from '../../config/colors';

export default function Title({text = 'Sample Text', customStyle}) {
  return <Text style={[styles.textStyle, customStyle]}>{text}</Text>;
}

const styles = StyleSheet.create({
  textStyle: {
    fontFamily: PRIMARY_FONT,
    fontSize: 52,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: -0.52,
    textAlign: 'center',
    color: colors.black,
  },
});
