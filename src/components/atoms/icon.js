import React from 'react';
import {Image, TouchableHighlight, StyleSheet} from 'react-native';

export default function IconComponent({icon}) {
  return (
    <TouchableHighlight style={styles.touchableWrapper}>
      <Image source={icon} style={styles.image} />
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  touchableWrapper: {
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
});
