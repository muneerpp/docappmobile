import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import colors from '../../config/colors';

export default function BottomSheetHeader({title}) {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.white,
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  title: {
    fontSize: 17,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
    // marginBottom: 20,
  },
});
