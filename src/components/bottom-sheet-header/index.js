import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import colors from '../../config/colors';

export default function BottomSheetHeader({title}) {
  return (
    <View style={styles.wrapper}>
      <View
        style={{
          width: 48,
          height: 6,
          borderRadius: 3,
          backgroundColor: '#e0e0e0',
          alignSelf: 'center',
          marginBottom: 20,
        }}
      />
      <Text style={styles.title}>{title}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: colors.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: -10,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f6f6f6',
  },
  title: {
    fontSize: 17,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 20,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
    // marginBottom: 20,
  },
});
