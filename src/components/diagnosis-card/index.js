import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import Image from 'react-native-fast-image';
import {ICON_EDIT, ICON_DELETE, ICON_APPROVE} from '../../assets/images';
import colors from '../../config/colors';
import {titleCase} from '../../utils/helpers';
import IconComponent from '../icon-component';
let imageurl =
  'https://c.ndtvimg.com/b2r2clqo_urinary-tract-infection_625x300_30_July_18.jpg';

export default function DiagnosisCard({name, onDelete, onApprove, onEdit}) {
  return (
    <View style={styles.wrapper}>
      <Image source={{uri: imageurl}} style={styles.diagImage} />

      <View style={styles.dataWrapper}>
        <Text style={styles.diagName}>{titleCase(name)}</Text>
        <Text style={styles.diagDescription}>
          An infection in any part of the urinary system, the kidneys, bladder
          or urethra.
        </Text>
      </View>

      <View style={styles.diagActionsWrapper}>
        {onApprove ? (
          <IconComponent
            icon={ICON_APPROVE}
            size={20}
            color={colors.white}
            outerStyle={styles.iconOuter}
          />
        ) : null}
        {onEdit ? (
          <IconComponent
            icon={ICON_EDIT}
            size={20}
            color={colors.white}
            outerStyle={styles.iconOuter}
          />
        ) : null}
        {onDelete ? (
          <IconComponent
            icon={ICON_DELETE}
            size={20}
            color={colors.white}
            outerStyle={styles.iconOuter}
            onPress={onDelete}
          />
        ) : null}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    borderRadius: 16,
    backgroundColor: colors.veryLightPink,
    shadowColor: 'rgba(0, 64, 128, 0.04)',
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 2,
    shadowOpacity: 1,
    marginHorizontal: 5,
    marginVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 6,
    paddingVertical: 10,
  },
  diagImage: {
    width: 100,
    height: 100,
    borderRadius: 8,
  },
  dataWrapper: {
    marginLeft: 12,
    flex: 1,
    height: 100,
  },
  diagName: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 18,
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.topaz,
  },
  diagDescription: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
    marginTop: 12,
  },
  diagActionsWrapper: {
    width: 40,
    height: 100,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  iconOuter: {
    width: 24,
    height: 24,
    backgroundColor: colors.lightGrey,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 8,
  },
});
