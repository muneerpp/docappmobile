import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import IconComponent from '../components/icon-component';
import {ACCOUNT_INACTIVE, CONSULT_INACTIVE, HOME_INACTIVE} from 'images';
import HomeScreen from '../containers/home-screen/home-screen.container';
import Account from '../containers/account/account';
import {NOTIFICATIONS} from '../assets/images';
import Notifications from '../containers/notifications/notifications';

const Tab = createBottomTabNavigator();

const HomeTabs = () => (
  <Tab.Navigator
    // initialRouteName="Account"
    tabBarOptions={{
      activeTintColor: 'tomato',
      inactiveTintColor: 'gray',
    }}
    screenOptions={({route, navigation}) => ({
      tabBarIcon: ({focused, color, size}) => {
        let iconName;
        if (route.name === 'Home') {
          iconName = HOME_INACTIVE;
          // iconName = focused
          //   ? 'ios-information-circle'
          //   : 'ios-information-circle-outline';
        } else if (route.name === 'Consult') {
          iconName = CONSULT_INACTIVE;
        } else if (route.name === 'Notifications') {
          iconName = NOTIFICATIONS;
        } else if (route.name === 'Account') {
          iconName = ACCOUNT_INACTIVE;
        }

        return (
          <IconComponent
            icon={iconName}
            size={20}
            style={{
              paddingVertical: 10,
              tintColor: focused ? '#12B2B3' : 'gray',
            }}
            onPress={() => {
              navigation.navigate(route.name);
            }}
          />
        );
      },
      // tabBarButton: (props) => {
      //   debugger;
      //   return <Text>Test</Text>;
      // },
    })}>
    <Tab.Screen name="Home" component={HomeScreen} />
    <Tab.Screen name="Account" component={Account} />
    <Tab.Screen name="Notifications" component={Notifications} />
  </Tab.Navigator>
);

export default HomeTabs;
