import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {connect} from 'react-redux';
import idx from 'idx';
import Home from './TabNavigation';
import ChooseDoctor from '../screens/choose-doctor';
import ChatScreen from '../screens/chat-screen';
import ConsultOnline from '../containers/consult-online/consult-online.container';
import SplashScreen from '../containers/splashscreen/splash-screen.container';
import AuthStack from './auth.stack';
import Onboarding from './onboarding.stack';
import MedicalCard from '../containers/medical-card';
import EditProfile from '../containers/edit-profile/edit-profile';
import CaseDetails from '../containers/case-details/case-details';

const Stack = createStackNavigator();

class Main extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{headerShown: false}}
          initialRouteName="SplashScreen">
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
          <Stack.Screen name="AuthStack" component={AuthStack} />
          <Stack.Screen name="Onboarding" component={Onboarding} />
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="ChooseDoctor" component={ChooseDoctor} />
          <Stack.Screen name="ConsultOnline" component={ConsultOnline} />
          <Stack.Screen name="ChatScreen" component={ChatScreen} />
          <Stack.Screen name="MedicalCard" component={MedicalCard} />
          <Stack.Screen name="EditProfile" component={EditProfile} />
          <Stack.Screen name="CaseDetails" component={CaseDetails} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

const mapStateToProps = (state) => {
  let promises = idx(state, (_) => _.promises.IN_PROGRESS);
  return {
    promises,
  };
};

export default connect(mapStateToProps, null)(Main);
