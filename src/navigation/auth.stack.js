import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Introduction from '../containers/introduction/introduction';
import RequestOTP from '../containers/request-otp/request-otp.container';
import VerifyOTP from '../containers/verify-otp/verify-otp.container';

const Stack = createStackNavigator();

export default function AuthStack() {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="Introduction">
      <Stack.Screen name="Introduction" component={Introduction} />
      <Stack.Screen name="RequestOTP" component={RequestOTP} />
      <Stack.Screen name="VerifyOTP" component={VerifyOTP} />
    </Stack.Navigator>
  );
}
