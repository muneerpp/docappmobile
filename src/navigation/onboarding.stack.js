import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Introduction from '../containers/onboarding/introduction';
import BasicData from '../containers/onboarding/basic-data';
import HealthData from '../containers/onboarding/health-data';

const Stack = createStackNavigator();

export default function AuthStack() {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="Introduction">
      <Stack.Screen name="Introduction" component={Introduction} />
      <Stack.Screen name="BasicData" component={BasicData} />
      <Stack.Screen name="HealthData" component={HealthData} />
      <Stack.Screen name="BMI" component={Introduction} />
      <Stack.Screen name="Confirm" component={Introduction} />
    </Stack.Navigator>
  );
}
