import {StyleSheet} from 'react-native';
import {ifIphoneX} from 'react-native-iphone-x-helper';
const styles = StyleSheet.create({
  screenWrapper: {
    ...ifIphoneX({
      paddingTop: 50,
    }),
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default styles;
