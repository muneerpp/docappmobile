import {Easing} from 'react-native-reanimated';
import {BottomSheetOverlay} from '@gorhom/bottom-sheet';
import BottomSheetHandle from '../components/bottom-sheet-handle';

const defaultBottomSheetConfigs = {
  snapPoints: [0, '60%'],
  initialSnapIndex: 1,
  animationDuration: 425,
  animationEasing: Easing.out(Easing.cubic),
  handleComponent: BottomSheetHandle,
  overlayComponent: BottomSheetOverlay,
  overlayOpacity: 0.75,
  dismissOnOverlayPress: true,
};

export default defaultBottomSheetConfigs;
