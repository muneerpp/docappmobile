import _ from 'lodash';
let tax = 18;
import i18n from 'react-native-i18n';

export const getCartTotal = (cart) => {
  let cartMainItemTotal = 0;

  if (cart && cart.length > 0) {
    cart.forEach((element) => {
      let elementTotal = element.mrp * element.count;
      cartMainItemTotal = cartMainItemTotal + elementTotal;
    });
  }

  return cartMainItemTotal;
};

export const formatNumber = (number) => parseFloat(number).toFixed(2);

export const getTaxAmount = (amount) => amount * (tax / 100);

export const validateAddress = (values) => {
  let error = {};

  if (!values) {
    error._error = 'No Values Added';
  }

  if (!values.name) {
    error._error = 'Name is required';
  }

  if (!values.locality) {
    error._error = 'Name is required';
  }

  if (!values.mobile_no) {
    error._error = 'Mobile Number is required';
  }

  if (!values.type) {
    error._error = 'Address type is required';
  }

  if (!values.latitude) {
    error._error = 'Latitude is required';
  }

  if (!values.longitude) {
    error._error = 'Longitude is required';
  }

  if (!values.city) {
    error._error = 'City is required';
  }

  if (!values.state) {
    error._error = 'State is required';
  }

  if (!values.pincode) {
    error._error = 'Pincode is required';
  }

  return error;
};

export const flattenProductList = (productList = []) => {
  const products = [];
  let totalCount = 0;
  let totalPrice = 0;
  productList.forEach((product) =>
    (product.variants || []).forEach((obj) => {
      const currentProduct = Object.assign({}, product, {variants: obj});
      if (currentProduct.variants.itemCount > 0) {
        products.push(currentProduct);
        totalCount += currentProduct.variants.itemCount;
        const {discountedUnitPrice, unitPrice} = currentProduct.variants;
        const currentPrice = discountedUnitPrice || unitPrice;
        totalPrice += currentPrice * currentProduct.variants.itemCount;
      }
    }),
  );
  return {products, totalCount, totalPrice};
};

export const formatMoney = (amount) =>
  i18n.toCurrency(amount / 100, {unit: '₹ '});

export const titleCase = (str) => (str ? _.startCase(_.toLower(str)) : '');
