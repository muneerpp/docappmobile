import React, {Component} from 'react';
import {Provider} from 'react-redux';
import GlobalFont from 'react-native-global-font';
import {Fonts} from './utils/constants';
import Navigation from './navigation';
import configureStore from './redux/store';
import I18n, {getLanguages} from 'react-native-i18n';

const {store} = configureStore();
console.disableYellowBox = true;
I18n.fallbacks = true;

// Available languages
I18n.translations = {
  en: require('./i18n/locales/en.json'),
  ml: require('./i18n/locales/ml.json'),
};

I18n.locale = 'en';

export class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    );
  }
}

export default App;
