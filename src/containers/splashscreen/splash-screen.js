import React, {Component} from 'react';
import {View} from 'react-native';
import {connect} from 'react-redux';
import styles from './styles';
import idx from 'idx';
import {setAuthHeaders} from 'api';

// const SPLASH_IMAGE = require('images/pembina1024.png');

export class SplashScreen extends Component {
  componentDidMount() {
    setTimeout(() => {
      let {accessToken, profiles} = this.props;
      // add extra method to validate access token here
      if (accessToken) {
        setAuthHeaders({accessToken});
        if (profiles.length < 1) {
          // check if user has any profiles
          this.props.navigation.replace('Onboarding');
          return;
        }
        this.props.navigation.replace('Home');
      } else {
        setAuthHeaders('');
        this.props.navigation.replace('AuthStack');
      }
    }, 100);
  }

  render() {
    return (
      <View style={styles.wrapper}>
        {/* <Image style={styles.image} source={SPLASH_IMAGE} /> */}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let {accessToken} = idx(state, (_) => _.user.userData) || {};
  let {profiles = []} = idx(state, (_) => _.user) || {};
  return {
    accessToken,
    profiles,
  };
};

export default connect(mapStateToProps, null)(SplashScreen);
