import {createAction} from 'redux-actions';
import {post} from 'api';
import idx from 'idx';
import {USER_SERVICE} from '../../redux/api/service.names';
import {
  CREATE_PROFILE_REQUEST,
  CREATE_PROFILE_SUCCESS,
  CREATE_PROFILE_FAILURE,
} from './onboarding.actions.const';

const createProfileRequest = createAction(CREATE_PROFILE_REQUEST);
const createProfileSuccess = createAction(CREATE_PROFILE_SUCCESS);
const createProfileFailed = createAction(CREATE_PROFILE_FAILURE);

export function createProfile(config, payload) {
  return async (dispatch) => {
    dispatch(createProfileRequest());
    let response;
    let error;

    try {
      const res = await post(
        `${config.baseURL}`,
        `${`/${USER_SERVICE}/patient`}`,
        payload,
      );
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(createProfileSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(createProfileFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(createProfileFailed(error));
    }

    return {response, error};
  };
}
