import React from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import ScreenWrapper from '../../components/screen-wrapper';
import OnboardingHeader from './header';
import {ONBOARDING_INTRO} from '../../assets/images';
import colors from '../../config/colors';
import Button from '../../components/button';

export default function Introduction({navigation}) {
  return (
    <ScreenWrapper>
      <OnboardingHeader stepCount={7} stepNumber={1} />
      <View style={styles.wrapper}>
        <Image
          source={ONBOARDING_INTRO}
          resizeMode="contain"
          style={{
            height: heightPercentageToDP('30%'),
          }}
        />

        <View style={styles.textsWrapper}>
          <Text style={styles.title}>
            We would require few of your information to create your health
            profile
          </Text>

          <Text style={styles.subText}>
            We promise , we would keep this information confidential
          </Text>
        </View>
        <View style={styles.buttonsWrapper}>
          <Button
            label="Continue"
            color={colors.azul}
            onPress={() => navigation.navigate('BasicData')}
          />
          <TouchableOpacity
            style={styles.skipButtonWrapper}
            onPress={() => navigation.replace('Home')}>
            <Text style={styles.skipButton}>Skip for now</Text>
          </TouchableOpacity>
        </View>
      </View>
    </ScreenWrapper>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    paddingHorizontal: widthPercentageToDP(5),
    alignItems: 'center',
    flex: 1,
    paddingBottom: 40,
  },
  title: {
    fontSize: 18,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: '#333333',
    marginBottom: heightPercentageToDP(2),
    lineHeight: 26,
  },
  subText: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#727272',
  },
  buttonsWrapper: {
    width: widthPercentageToDP('90%'),
    height: heightPercentageToDP('12%'),
    justifyContent: 'space-around',
  },
  textsWrapper: {
    flex: 1,
  },
  skipButton: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#727272',
    lineHeight: 30,
  },
  skipButtonWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
