/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import Button from '../../components/button';
import ScreenWrapper from '../../components/screen-wrapper';
import OnboardingHeader from './header';
import ip from 'icepick';
import SimpleToast from 'react-native-simple-toast';
import colors from '../../config/colors';
import {useDispatch, useSelector} from 'react-redux';
import {createProfile} from './onboarding.actions';

const BLOOD_GROUPS = [
  {
    id: 1,
    text: 'A+',
  },
  {
    id: 2,
    text: 'A-',
  },
  {
    id: 3,
    text: 'B+',
  },
  {
    id: 4,
    text: 'B-',
  },
  {
    id: 5,
    text: 'O-',
  },
  {
    id: 6,
    text: 'O+',
  },
  {
    id: 7,
    text: 'AB+',
  },
  {
    id: 8,
    text: 'AB-',
  },
];

const MEDICAL_HISTORY = [
  {
    id: 1,
    text: 'Arthritis',
  },
  {
    id: 2,
    text: 'Asthma',
  },
  {
    id: 3,
    text: 'Cancer',
  },
  {
    id: 4,
    text: 'Bronchitis',
  },
  {
    id: 5,
    text: 'Heart Attack',
  },
  {
    id: 6,
    text: 'Diabetes',
  },
  {
    id: 7,
    text: 'Obese',
  },
  {
    id: 8,
    text: 'More+',
  },
];

export const BloodGroupPicker = ({onSelect, title}) => {
  const [selectedGroup, selectGroup] = useState(null);
  useEffect(() => {
    onSelect(selectedGroup);
  }, [selectedGroup]);
  return (
    <View>
      {title ? <Text style={styles.subTitle}>Blood Type</Text> : null}
      <View style={styles.bloodTypesWrapper}>
        {BLOOD_GROUPS.map((group) => (
          <TouchableOpacity
            onPress={() => selectGroup(group)}
            style={[
              styles.chipWrapper,
              // eslint-disable-next-line react-native/no-inline-styles
              {
                backgroundColor:
                  selectedGroup && selectedGroup.id === group.id
                    ? '#4a5cd0'
                    : colors.white,
                borderColor:
                  selectedGroup && selectedGroup.id === group.id
                    ? colors.azul
                    : colors.veryLightPink,
              },
            ]}>
            <Text
              style={[
                styles.chipText,
                // eslint-disable-next-line react-native/no-inline-styles
                {
                  color:
                    selectedGroup && selectedGroup.id === group.id
                      ? colors.white
                      : '#c3c3c3',
                },
              ]}>
              {group.text}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
};

export const MedicalIssuesPicker = ({onSelect}) => {
  const [selectedGroup, selectGroup] = useState([]);

  const selectItem = (item) => {
    if (item.id === 8) {
      SimpleToast.show('Picker');
      return;
    }

    if (!selectedGroup.some((obj) => obj.id === item.id)) {
      let items = ip.push(selectedGroup, item);
      selectGroup(items);
    } else {
      let items = selectedGroup.filter((obj) => obj.id !== item.id);
      selectGroup(items);
    }
  };

  useEffect(() => {
    onSelect(selectedGroup);
  }, [selectedGroup]);

  return (
    <View>
      <Text style={styles.subTitle}>Medical Problems</Text>
      <View style={styles.bloodTypesWrapper}>
        {MEDICAL_HISTORY.map((group) => (
          <TouchableOpacity
            onPress={() => selectItem(group)}
            style={[
              styles.chipWrapper,
              // eslint-disable-next-line react-native/no-inline-styles
              {
                backgroundColor: selectedGroup.some(
                  (obj) => obj.id === group.id,
                )
                  ? '#4a5cd0'
                  : colors.white,
                borderColor:
                  selectedGroup && selectedGroup.id === group.id
                    ? colors.azul
                    : colors.veryLightPink,
              },
            ]}>
            <Text
              style={[
                styles.chipText,
                // eslint-disable-next-line react-native/no-inline-styles
                {
                  color:
                    selectedGroup && selectedGroup.id === group.id
                      ? colors.white
                      : '#c3c3c3',
                },
              ]}>
              {group.text}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    </View>
  );
};

export default function HealthData({navigation, route}) {
  const config = useSelector((state) => state.appStateReducer.config);
  const userData = useSelector((state) => state.user.userData);
  const dispatch = useDispatch();

  const [bloodGroup, setBloodGroup] = useState(null);
  const [conditions, setConditions] = useState(null);

  const onSubmitRequest = async () => {
    let {firstName, lastName, birthDay} = route.params || {};
    let payload = {
      userId: userData.id,
      name: firstName,
      surname: lastName,
      phone: userData.mobileNumber,
      image: 'string',
      address: 'test address',
      gender: 'MALE',
      dob: birthDay,
      bloodGroup: bloodGroup,
      alergies: 'String',
      previousCondition: conditions,
      parentRelation: 'SELF',
    };
    let {response, error} = await dispatch(createProfile(config, payload));
    navigation.replace('Home');
  };

  return (
    <ScreenWrapper backgroundColor={colors.white}>
      <OnboardingHeader stepCount={3} stepNumber={1} />
      <ScrollView
        contentContainerStyle={styles.scrollView}
        keyboardShouldPersistTaps="handled">
        <Text style={styles.title}>Time to fill your medical history</Text>

        <View style={styles.inputWrapper}>
          <BloodGroupPicker onSelect={(group) => setBloodGroup(group)} />
          <MedicalIssuesPicker onSelect={(group) => setConditions(group)} />
        </View>
        <Button label="Next" color={colors.azul} onPress={onSubmitRequest} />
        <KeyboardSpacer />
      </ScrollView>
    </ScreenWrapper>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: widthPercentageToDP(5),
    paddingBottom: heightPercentageToDP(5),
  },
  title: {
    fontSize: 21,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
    marginTop: heightPercentageToDP(3),
  },
  subTitle: {
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
    marginTop: heightPercentageToDP(1),
    width: widthPercentageToDP(80),
  },
  errorMessage: {
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.brownishPink,
    marginTop: heightPercentageToDP(1),
    width: widthPercentageToDP(80),
  },
  inputWrapper: {
    flex: 1,
    marginTop: heightPercentageToDP(2),
  },
  bloodTypesWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: heightPercentageToDP(1),
    alignItems: 'center',
  },
  chipWrapper: {
    height: 29,
    marginRight: 10,
    marginBottom: 20,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    width: widthPercentageToDP('20%'),
  },
  chipText: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
  },
});
