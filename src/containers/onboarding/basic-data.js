import React, {useState, createRef, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import Button from '../../components/button';
import InputBox from '../../components/input-box';
import ScreenWrapper from '../../components/screen-wrapper';
import OnboardingHeader from './header';
import DateTimePicker from '@react-native-community/datetimepicker';
import SimpleToast from 'react-native-simple-toast';
import colors from '../../config/colors';
import BottomSheet, {
  BottomSheetView,
  BottomSheetBackdrop,
} from '@gorhom/bottom-sheet';
import defaultBottomSheetConfigs from '../../utils/bottom-sheet-config';
import {Easing} from 'react-native-reanimated';

export default function BasicData({navigation, requestOTP, config}) {
  const datePlaceHolder = new Date();
  const [firstName, setFirstName] = useState(null);
  const [lastName, setLastName] = useState(null);
  const [birthDay, setBirthDay] = useState(datePlaceHolder);
  const [nextEnabled, setNextButton] = useState(false);

  const birthdaySheet = createRef();

  useEffect(() => {
    if (firstName && lastName && birthDay !== datePlaceHolder) {
      setNextButton(true);
    } else {
      setNextButton(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [firstName, lastName, birthDay]);

  return (
    <ScreenWrapper backgroundColor={colors.white}>
      <OnboardingHeader stepCount={3} stepNumber={1} />
      <ScrollView
        contentContainerStyle={styles.scrollView}
        keyboardShouldPersistTaps="handled">
        <Text style={styles.title}>Tell us more about you</Text>
        <View style={styles.inputWrapper}>
          <InputBox
            onChangeText={(name) => setFirstName(name)}
            placeholder="First Name"
            value={firstName}
            maxLength={32}
          />
          <View
            style={{
              height: heightPercentageToDP(5),
            }}
          />

          <InputBox
            onChangeText={(name) => setLastName(name)}
            placeholder="Last Name"
            value={lastName}
            maxLength={32}
          />
          <View
            style={{
              height: heightPercentageToDP(5),
            }}
          />
          <TouchableOpacity
            onPress={() => birthdaySheet.current.snapTo(1)}
            style={styles.birthDayPickerWrapper}>
            {birthDay === datePlaceHolder ? (
              <Text style={styles.birthDayPlaceHolder}>Birth Day</Text>
            ) : (
              <Text style={styles.birthDay}>
                {new Date(birthDay).toLocaleDateString()}
              </Text>
            )}
          </TouchableOpacity>
        </View>
        {nextEnabled ? (
          <Button
            label="Next"
            color={colors.azul}
            onPress={() =>
              navigation.navigate('HealthData', {firstName, lastName, birthDay})
            }
          />
        ) : (
          <Button
            label="Next"
            color={colors.paleGrey}
            onPress={() => SimpleToast.show('Please enter all required fields')}
          />
        )}

        <KeyboardSpacer />
      </ScrollView>

      <BottomSheet
        ref={birthdaySheet}
        snapPoints={[-50, heightPercentageToDP(40)]}
        initialSnapIndex={-1}
        animationDuration={defaultBottomSheetConfigs.animationDuration}
        animationEasing={Easing.out(Easing.back(1))}
        backdropComponent={BottomSheetBackdrop}
        onChange={(index) => {}}>
        <BottomSheetView>
          <View style={styles.datePickerWrapper}>
            {birthDay ? (
              <DateTimePicker
                testID="dateTimePicker"
                value={birthDay}
                mode="date"
                is24Hour={true}
                display="default"
                onChange={(_, date) => {
                  setBirthDay(date);
                }}
              />
            ) : null}
            <Button
              label="Done"
              color={colors.azul}
              onPress={() => birthdaySheet.current.snapTo(0)}
            />
          </View>
        </BottomSheetView>
      </BottomSheet>
    </ScreenWrapper>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: widthPercentageToDP(5),
    paddingBottom: heightPercentageToDP(5),
  },
  title: {
    fontSize: 21,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
    marginTop: heightPercentageToDP(3),
  },
  subTitle: {
    opacity: 0.35,
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
    marginTop: heightPercentageToDP(1),
    width: widthPercentageToDP(80),
  },
  errorMessage: {
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.brownishPink,
    marginTop: heightPercentageToDP(1),
    width: widthPercentageToDP(80),
  },
  inputWrapper: {
    flex: 1,
    marginTop: heightPercentageToDP(2),
  },
  birthDayPickerWrapper: {
    height: 46,
    borderRadius: 8,
    backgroundColor: 'rgba(255, 255, 255, 0)',
    borderStyle: 'solid',
    borderWidth: 2,
    borderColor: '#0073fa',
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  birthDayPlaceHolder: {
    opacity: 0.35,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
  },
  birthDay: {
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
  },
  datePickerWrapper: {
    backgroundColor: colors.white,
    paddingHorizontal: 10,
  },
});
