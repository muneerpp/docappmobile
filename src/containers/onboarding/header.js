import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {LEFT} from '../../assets/images';
import IconComponent from '../../components/icon-component';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import {withNavigation} from '@react-navigation/compat';
import colors from '../../config/colors';

function OnboardingHeader({stepCount, stepNumber, navigation}) {
  let isLast = stepCount === stepNumber;
  return (
    <View style={styles.wrapper}>
      <View style={styles.leftWrapper}>
        <IconComponent
          icon={LEFT}
          size={24}
          onPress={() => navigation.goBack()}
        />
      </View>
      <View style={styles.contentWrapper}>
        <Text style={styles.stepsText}>
          STEP {stepNumber} / {stepCount}
        </Text>
        <View style={styles.stepsWrapper}>
          <View style={styles.stepsFull} />
          <View
            style={[
              styles.stepsFilled,
              // eslint-disable-next-line react-native/no-inline-styles
              {
                width: 133 * (stepNumber / stepCount),
                borderTopRightRadius: isLast ? 6 : 0,
                borderBottomRightRadius: isLast ? 6 : 0,
              },
            ]}
          />
        </View>
      </View>

      <View style={styles.rightWrapper} />
    </View>
  );
}

export default withNavigation(OnboardingHeader);

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    paddingVertical: heightPercentageToDP(2),
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  leftWrapper: {
    width: 40,
  },
  rightWrapper: {width: 40},
  contentWrapper: {
    flex: 1,
    alignItems: 'center',
  },
  stepsText: {
    fontSize: 10,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#999999',
    marginBottom: heightPercentageToDP(0.5),
  },
  stepsFull: {
    width: 133,
    height: 5,
    backgroundColor: '#e8e8e8',
    borderTopRightRadius: 6,
    borderBottomRightRadius: 6,
  },
  stepsFilled: {
    height: 5,
    backgroundColor: colors.azure,
    position: 'absolute',
    left: 0,
    borderTopLeftRadius: 6,
    borderBottomLeftRadius: 6,
  },
  stepsWrapper: {
    flexDirection: 'row',
  },
});
