import React from 'react';
import {
  Dimensions,
  ImageBackground,
  Text,
  View,
  Image,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import SafeWrapper from '../../components/hoc/SafeWrapper';
import {CHAT_WITH_DOCTOR, HOME_BG, HOSPITAL, MEDICINE} from 'images';
import colors from '../../config/colors';
import SpecialitiesGrid from '../../components/molecules/specialities-grid';
import SymptomsGrid from '../../components/molecules/symptoms-grid';
import {useEffect} from 'react';
import CaseStatusCard from '../../components/case-status-card';

const {width} = Dimensions.get('window');
const HEADER_ITEMS = [
  {
    text: 'Consult Online',
    image: CHAT_WITH_DOCTOR,
    id: 'ConsultOnline',
  },
  {
    text: 'Book Lab test',
    image: HOSPITAL,
    id: 'ConsultOnline',
  },
  {
    text: 'Buy Meidicine',
    image: MEDICINE,
    id: 'ConsultOnline',
  },
];

const Header = ({onPress}) => (
  <View style={styles.headerWrapper}>
    {HEADER_ITEMS.map((item) => (
      <TouchableOpacity
        style={styles.headerItemWrapper}
        onPress={() => onPress(item.id)}>
        <Image source={item.image} style={styles.headerIcon} />
        <Text style={styles.headerItemText}>{item.text}</Text>
      </TouchableOpacity>
    ))}
  </View>
);

export default function HomeScreen({
  navigation,
  getUserData,
  getProfiles,
  config,
  getCasesList,
  cases,
}) {
  const onHeaderItemPressed = (item) => {
    navigation.navigate(item);
  };

  // get user data and all profiles

  // const getUser = async () => {
  //   await getUserData(config);
  // };

  const getProfilesList = async () => {
    await getProfiles(config);
  };

  const getCasesListRequest = async () => {
    await getCasesList(config);
  };

  useEffect(() => {
    // getUser();
    getProfilesList();
    getCasesListRequest();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ImageBackground style={{width: '100%', height: '100%'}} source={HOME_BG}>
      <SafeWrapper>
        <Header onPress={onHeaderItemPressed} />
        <ScrollView>
          <View style={styles.contentWrapper}>
            <CaseStatusCard
              cases={cases}
              onSelectCase={(caseDetails) =>
                navigation.navigate('CaseDetails', {caseDetails: caseDetails})
              }
            />
            <SymptomsGrid />
            <SpecialitiesGrid />
          </View>
        </ScrollView>
      </SafeWrapper>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  headerWrapper: {
    width: width - 40,
    height: 82,
    borderRadius: 12,
    backgroundColor: colors.white,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  headerItemWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerIcon: {
    width: 48,
    height: 48,
  },
  headerItemText: {
    fontSize: 10,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.blackTwo,
    paddingVertical: 3,
  },
  contentWrapper: {
    height: '100%',
    marginTop: 20,
    backgroundColor: colors.white,
    shadowColor: 'rgba(0, 0, 0, 0.08)',
    shadowOffset: {
      width: 0,
      height: 18,
    },
    shadowRadius: 40,
    shadowOpacity: 1,
    borderTopLeftRadius: 32,
    borderTopRightRadius: 32,
    paddingVertical: 20,
    paddingHorizontal: 20,
  },
});
