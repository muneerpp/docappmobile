import {createAction} from 'redux-actions';
import {get} from 'api';
import {
  GET_USER_DATA_FAILURE,
  GET_USER_DATA_REQUEST,
  GET_USER_DATA_SUCCESS,
  GET_PROFILES_LIST_REQUEST,
  GET_PROFILES_LIST_SUCCESS,
  GET_PROFILES_LIST_FAILURE,
  GET_CASES_LIST_REQUEST,
  GET_CASES_LIST_SUCCESS,
  GET_CASES_LIST_FAILURE,
} from './home-screen.actions.const';
import idx from 'idx';
import {MEDICAL_CORE} from '../../redux/api/service.names';

const getUserDataRequest = createAction(GET_USER_DATA_REQUEST);
const getUserDataSuccess = createAction(GET_USER_DATA_SUCCESS);
const getUserDataFailed = createAction(GET_USER_DATA_FAILURE);

export function getUserData(config) {
  return async (dispatch) => {
    dispatch(getUserDataRequest());
    let response;
    let error;

    try {
      const res = await get(`${config.baseURL}`, `${'/user/auth/user-data/2'}`);
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(getUserDataSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(getUserDataFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(getUserDataFailed(error));
    }

    return {response, error};
  };
}

const getProfilesRequest = createAction(GET_PROFILES_LIST_REQUEST);
const getProfilesSuccess = createAction(GET_PROFILES_LIST_SUCCESS);
const getProfilesFailed = createAction(GET_PROFILES_LIST_FAILURE);

export function getProfiles(config) {
  return async (dispatch) => {
    dispatch(getProfilesRequest());
    let response;
    let error;

    try {
      const res = await get(`${config.baseURL}`, `${'/user/patient/'}`);
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(getProfilesSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(getProfilesFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(getProfilesFailed(error));
    }

    return {response, error};
  };
}

const getCasesListRequest = createAction(GET_CASES_LIST_REQUEST);
const getCasesListSuccess = createAction(GET_CASES_LIST_SUCCESS);
const getCasesListFailed = createAction(GET_CASES_LIST_FAILURE);

export function getCasesList(config) {
  return async (dispatch) => {
    dispatch(getCasesListRequest());
    let response;
    let error;

    try {
      const res = await get(
        `${config.baseURL}`,
        `${`/${MEDICAL_CORE}/case/cases/`}`,
      );
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(getCasesListSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(getCasesListFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(getCasesListFailed(error));
    }

    return {response, error};
  };
}
