import {connect} from 'react-redux';
import HomeScreen from './home-screen';
import idx from 'idx';
import {getUserData, getProfiles, getCasesList} from './home-screen.actions';

const mapStateToProps = (state) => {
  let config = idx(state, (_) => _.appStateReducer.config) || {};
  let cases = idx(state, (_) => _.consultation.cases) || {};
  return {
    config,
    cases,
  };
};

export default connect(mapStateToProps, {
  getUserData,
  getProfiles,
  getCasesList,
})(HomeScreen);
