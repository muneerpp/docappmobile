import {createAction} from 'redux-actions';
import {get} from 'api';

import idx from 'idx';
import {MEDICAL_CORE} from '../../redux/api/service.names';
import {
  GET_CASE_DETAILS_REQUEST,
  GET_CASE_DETAILS_SUCCESS,
  GET_CASE_DETAILS_FAILURE,
} from './case-details.actions.const';

const getCaseDetailsRequest = createAction(GET_CASE_DETAILS_REQUEST);
const getCaseDetailsSuccess = createAction(GET_CASE_DETAILS_SUCCESS);
const getCaseDetailsFailed = createAction(GET_CASE_DETAILS_FAILURE);

export function getCaseDetails(config, id) {
  return async (dispatch) => {
    dispatch(getCaseDetailsRequest());
    let response;
    let error;

    try {
      const res = await get(
        `${config.baseURL}`,
        `${`/${MEDICAL_CORE}/case/cases/${id}`}`,
      );
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(getCaseDetailsSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(getCaseDetailsFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(getCaseDetailsFailed(error));
    }

    return {response, error};
  };
}
