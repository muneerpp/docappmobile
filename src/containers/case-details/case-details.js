import React, {createRef, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

import {ADD_ICON, HELP} from 'images';

import HeaderNavigation from '../../components/molecules/header-navigation';
import PrescriptionCard from '../../components/prescription-card';
import CardWrapper from '../../components/molecules/card-wrapper';
import {titleCase} from '../../utils/helpers';
import idx from 'idx';
import {useDispatch, useSelector} from 'react-redux';
import IconComponent from '../../components/icon-component';
import DiagnosisCard from '../../components/diagnosis-card';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {getCaseDetails} from './case-details.actions';
import Button from '../../components/button';
import colors from '../../config/colors';

const AddButton = ({onPress}) => (
  <TouchableOpacity onPress={onPress}>
    <IconComponent icon={ADD_ICON} size={20} onPress={onPress} />
  </TouchableOpacity>
);

const DataEmpty = ({message}) => (
  <View style={styles.dataEmptyWrapper}>
    <Text>{message}</Text>
  </View>
);

export default function CaseDetails({route}) {
  const dispatch = useDispatch();
  const config = useSelector((state) => state.appStateReducer.config);

  useEffect(() => {
    let caseId = idx(route, (_) => _.params.caseDetails.id);
    dispatch(getCaseDetails(config, caseId));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const activeCase =
    useSelector((state) => state.consultation.activeCase) || {};
  const conditionOccurances =
    idx(activeCase, (_) => _.conditionOccurances) || [];
  const symptomOccurances = idx(activeCase, (_) => _.symptomOccurances) || [];
  const prescriptions = idx(activeCase, (_) => _.prescriptions) || [];
  return (
    <View style={{flex: 1}}>
      <HeaderNavigation title={(activeCase || {}).name} />
      <ScrollView>
        <View style={{flex: 1, paddingBottom: heightPercentageToDP(10)}}>
          <View style={styles.cardWrapper}>
            <CardWrapper
              title="Symptoms"
              icon={HELP}
              RightComponent={() => (
                <AddButton onPress={() => alert('test')} />
              )}>
              <View style={styles.symptomsWrapper}>
                {symptomOccurances.length > 0 ? (
                  symptomOccurances.map((symptom) => (
                    <View style={styles.symptomWrapper}>
                      <Text style={styles.symptomTitle}>
                        {titleCase(idx(symptom, (_) => _.symptom.name) || '')}
                      </Text>
                      <Text style={styles.symptomStartedAgo}>
                        {titleCase(idx(symptom, (_) => _.symptom.name) || '')}
                      </Text>
                    </View>
                  ))
                ) : (
                  <DataEmpty message="No Symptom has been added to your case" />
                )}
              </View>
            </CardWrapper>
          </View>
          <View style={styles.cardWrapper}>
            <CardWrapper title="Diagnosis" icon={HELP}>
              {conditionOccurances.length > 0 ? (
                conditionOccurances.map((condition) => (
                  <DiagnosisCard {...condition} />
                ))
              ) : (
                <DataEmpty message="No Diagnosis has been added yet by doctor" />
              )}
            </CardWrapper>
          </View>

          {activeCase.procedures && (
            <View style={styles.cardWrapper}>
              <CardWrapper title="Procedures" icon={HELP} />
            </View>
          )}

          {activeCase.chat && (
            <View style={styles.cardWrapper}>
              <CardWrapper title="Chat" icon={HELP} />
            </View>
          )}

          <View style={styles.cardWrapper}>
            <CardWrapper title="Prescription" icon={HELP}>
              {prescriptions.length > 0 ? (
                prescriptions.map((prescription) => (
                  <PrescriptionCard {...prescription} />
                ))
              ) : (
                <DataEmpty message="No prescriptions has been added to your case yet" />
              )}
            </CardWrapper>
          </View>

          <View style={styles.cardWrapper}>
            <CardWrapper title="Observation" icon={HELP}>
              <View
                style={{
                  paddingHorizontal: 10,
                  flex: 1,
                }}>
                <Text>
                  Observation Period : {activeCase.observationPeriod}{' '}
                  {activeCase.observationUnit || ''}
                </Text>
              </View>
              <View
                style={{
                  paddingHorizontal: widthPercentageToDP(2),
                  paddingVertical: 10,
                }}>
                <Button label="Mark As I am Fine" color={colors.azul} />
              </View>
            </CardWrapper>
          </View>

          {activeCase.notes && (
            <View style={styles.cardWrapper}>
              <CardWrapper title="Notes" icon={HELP} />
            </View>
          )}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  cardWrapper: {
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  symptomsWrapper: {
    paddingHorizontal: 24,
    marginTop: 10,
  },
  symptomWrapper: {
    height: 40,
    borderRadius: 4,
    backgroundColor: '#f6f6f6',
    marginBottom: 20,
    alignItems: 'center',
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  symptomTitle: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
  },
  symptomStartedAgo: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#9393aa',
  },
  addConditionWrapper: {
    height: 36,
    borderRadius: 8,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#e0e0e0',
    marginHorizontal: 26,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  addConditionText: {
    fontSize: 13,
    fontWeight: '500',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#9393aa',
    marginLeft: 10,
  },
  dataEmptyWrapper: {
    height: 150,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
