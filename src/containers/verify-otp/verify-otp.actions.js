import {createAction} from 'redux-actions';
import {post} from 'api';
import idx from 'idx';
import {USER_SERVICE} from '../../redux/api/service.names';
import {
  VERIFY_OTP_FAILURE,
  VERIFY_OTP_REQUEST,
  VERIFY_OTP_SUCCESS,
} from '../../redux/actions/user.actions.const';

const verifyOTPRequest = createAction(VERIFY_OTP_REQUEST);
const verifyOTPSuccess = createAction(VERIFY_OTP_SUCCESS);
const verifyOTPFailed = createAction(VERIFY_OTP_FAILURE);

export function verifyOTP(config, payload) {
  return async (dispatch) => {
    dispatch(verifyOTPRequest());
    let response;
    let error;

    try {
      const res = await post(
        `${config.baseURL}`,
        `${`/${USER_SERVICE}/auth/verifyOTP`}`,
        payload,
      );
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(verifyOTPSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(verifyOTPFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(verifyOTPFailed(error));
    }

    return {response, error};
  };
}
