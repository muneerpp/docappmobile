import React from 'react';
import {View, Image, Text, ScrollView, StyleSheet} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {APP_LOGO} from '../../assets/images';
import Button from '../../components/button';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import ScreenWrapper from '../../components/screen-wrapper';
import colors from '../../config/colors';
import {isIphoneX} from 'react-native-iphone-x-helper';
import idx from 'idx';
import SimpleToast from 'react-native-simple-toast';
import {setAuthHeaders} from 'api';

export default function VerifyOTP({
  route,
  navigation,
  verifyOTP,
  config,
  getProfiles,
  logout,
}) {
  const {phoneNumber = ''} = idx(route, (_) => _.params) || {};

  const verifyOTPRequest = async (otp) => {
    let payload = {
      mobileNumber: phoneNumber,
      isdCode: '',
      otp: otp,
    };
    let {error, response} = await verifyOTP(config, payload);
    // set auth header
    if (error) {
      let {message = 'Something went wrong , please try again'} =
        idx(error, (_) => _.data) || {};
      SimpleToast.show(message);
      return;
    }
    if (response) {
      if (response.userType !== 'END_USER') {
        SimpleToast.show('Please download care app');
        logout();
        setTimeout(() => {
          navigation.replace('SplashScreen');
        }, 100);
        return;
      }
      // get user profiles too
      setAuthHeaders({accessToken: response.accessToken});
      let data = await getProfiles(config);
      let profiles = idx(data, (_) => _.response) || [];
      if (profiles.length > 0) {
        navigation.replace('Home');
      } else {
        navigation.navigate('Onboarding');
      }
    }
  };

  return (
    <ScreenWrapper backgroundColor={colors.white}>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <Image
          style={{
            width: heightPercentageToDP(8),
            height: heightPercentageToDP(8),
          }}
          source={APP_LOGO}
        />
        <Text style={styles.title}>Check your message. 📦</Text>
        <Text style={styles.subTitle}>
          {`Enter the code we’ve sent to your phone number ${phoneNumber}`}
        </Text>
        <View style={styles.inputWrapper}>
          <OTPInputView
            style={styles.otpInput}
            pinCount={4}
            autoFocusOnLoad
            codeInputFieldStyle={styles.underlineStyleBase}
            codeInputHighlightStyle={styles.underlineStyleHighLighted}
            onCodeFilled={(code) => {
              verifyOTPRequest(code);
            }}
          />
        </View>
        <Button label="Verify" color={colors.azul} />
        <KeyboardSpacer />
      </ScrollView>
    </ScreenWrapper>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: widthPercentageToDP(5),
    paddingBottom: heightPercentageToDP(5),
  },
  title: {
    fontSize: 21,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
    marginTop: heightPercentageToDP(3),
  },
  subTitle: {
    opacity: 0.35,
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
    marginTop: heightPercentageToDP(1),
    width: widthPercentageToDP(80),
  },
  inputWrapper: {
    flex: 1,
    marginTop: heightPercentageToDP('15%'),
    alignItems: 'center',
  },
  otpInput: {
    width: widthPercentageToDP('60%'),
    height: widthPercentageToDP('7%'),
    marginRight: 10,
  },
  underlineStyleBase: {
    width: widthPercentageToDP('6%'),
    height: heightPercentageToDP('3.35%'),
    borderWidth: 0,
    borderBottomWidth: 2,
    fontSize: isIphoneX()
      ? widthPercentageToDP('4.3%')
      : widthPercentageToDP('4%'),
    fontWeight: 'bold',
    marginRight: 10,
    padding: 0,
    paddingBottom: 4,
  },
  underlineStyleHighLighted: {
    borderColor: colors.primaryPink,
    width: widthPercentageToDP('6%'),
    height: heightPercentageToDP('3.35%'),
    borderWidth: 0,
    borderBottomWidth: 2,
    fontWeight: 'bold',
  },
});
