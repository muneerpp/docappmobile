import {connect} from 'react-redux';
import VerifyOTP from './verify-otp';
import {verifyOTP} from './verify-otp.actions';
import {getProfiles} from '../home-screen/home-screen.actions';
import {logout} from '../account/account.actions';
import idx from 'idx';

const mapStateToProps = (state) => {
  let config = idx(state, (_) => _.appStateReducer.config) || {};
  return {
    config,
  };
};

export default connect(mapStateToProps, {
  verifyOTP,
  getProfiles,
  logout,
})(VerifyOTP);
