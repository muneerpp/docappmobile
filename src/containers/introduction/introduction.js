import React from 'react';
import {Text, Image, View, StyleSheet} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {ILLUS_WELCOME, APP_LOGO} from '../../assets/images';
import Button from '../../components/button';
import ScreenWrapper from '../../components/screen-wrapper';
import colors from '../../config/colors';

export default function Introduction({navigation}) {
  return (
    <ScreenWrapper backgroundColor={colors.white}>
      <View style={styles.wrapper}>
        <Image style={styles.logo} source={APP_LOGO} />
        <Text style={styles.welcomeText}>Welcome To Doctorhub</Text>
        <Text style={styles.welcomeSubText}>
          All your medical and health needs in one place.
        </Text>
        <View style={styles.illusWrapper}>
          <Image source={ILLUS_WELCOME} style={styles.illus} />
        </View>
        <View
          style={{
            width: widthPercentageToDP('90%'),
          }}>
          <Button
            label="Get Started"
            color={colors.azul}
            onPress={() => navigation.replace('RequestOTP')}
          />
        </View>
      </View>
    </ScreenWrapper>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: heightPercentageToDP(4),
    paddingBottom: heightPercentageToDP(4),
    flex: 1,
  },
  welcomeText: {
    fontSize: 18,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.blackTwo,
    marginVertical: heightPercentageToDP(1),
  },
  welcomeSubText: {
    opacity: 0.6,
    fontSize: 14,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'center',
    color: colors.blackTwo,
  },
  illus: {
    resizeMode: 'contain',
    marginVertical: heightPercentageToDP(5),
  },
  illusWrapper: {
    flex: 1,
  },
  logo: {
    width: heightPercentageToDP(12),
    height: heightPercentageToDP(12),
  },
});
