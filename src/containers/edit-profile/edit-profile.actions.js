import {createAction} from 'redux-actions';
import {put, post} from 'api';
import idx from 'idx';
import {USER_SERVICE} from '../../redux/api/service.names';
import axios from 'axios';
import {
  UPDATE_PATIENT_DATA_REQUEST,
  UPDATE_PATIENT_DATA_SUCCESS,
  UPDATE_PATIENT_DATA_FAILURE,
  SAVE_PATIENT_DATA_REQUEST,
  SAVE_PATIENT_DATA_SUCCESS,
  SAVE_PATIENT_DATA_FAILURE,
  UPLOAD_IMAGE_REQUEST,
  UPLOAD_IMAGE_SUCCESS,
  UPLOAD_IMAGE_FAILURE,
} from './edit-profile.actions.const';

const createPatientRequest = createAction(SAVE_PATIENT_DATA_REQUEST);
const createPatientSuccess = createAction(SAVE_PATIENT_DATA_SUCCESS);
const createPatientFailed = createAction(SAVE_PATIENT_DATA_FAILURE);

export function createPatient(config, payload) {
  return async (dispatch) => {
    dispatch(createPatientRequest());
    let response;
    let error;

    try {
      const res = await post(
        `${config.baseURL}`,
        `${`/${USER_SERVICE}/patient`}`,
        payload,
      );
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(createPatientSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(createPatientFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(createPatientFailed(error));
    }

    return {response, error};
  };
}

const updatePatientProfileRequest = createAction(UPDATE_PATIENT_DATA_REQUEST);
const updatePatientProfileSuccess = createAction(UPDATE_PATIENT_DATA_SUCCESS);
const updatePatientProfileFailed = createAction(UPDATE_PATIENT_DATA_FAILURE);

export function updatePatientProfile(config, id, payload) {
  return async (dispatch) => {
    dispatch(updatePatientProfileRequest());
    let response;
    let error;

    try {
      const res = await put(
        `${config.baseURL}`,
        `${`/${USER_SERVICE}/patient/${id}`}`,
        payload,
      );
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(updatePatientProfileSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(updatePatientProfileFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(updatePatientProfileFailed(error));
    }

    return {response, error};
  };
}

const uploadImageRequest = createAction(UPLOAD_IMAGE_REQUEST);
const uploadImageSuccess = createAction(UPLOAD_IMAGE_SUCCESS);
const uploadImageFailed = createAction(UPLOAD_IMAGE_FAILURE);

export function uploadImage(config, data, userId) {
  return async (dispatch) => {
    dispatch(uploadImageRequest());
    let response = null;
    let error = null;
    axios.defaults.headers.folderName = `user-image-${userId}`;
    try {
      const res = await axios.post(
        `${config.baseURL}${'/content/images/upload'}`,
        data,
      );
      // const res = await axios.post('http://localhost:3000/images/upload', data);
      if (res) {
        response = res.data;
        dispatch(uploadImageSuccess(res));
      }
    } catch (ex) {
      error = ex;
      dispatch(uploadImageFailed(ex));
      error = ex;
    }

    return {response, error};
  };
}
