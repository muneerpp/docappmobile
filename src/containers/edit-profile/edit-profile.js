import React, {useState, createRef, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Header from '../../components/molecules/header-navigation';
import Image from 'react-native-fast-image';
import {
  DOCTOR_IMAGE_PLACEHOLDER,
  ICON_EDIT,
  ICON_DROPDOWN,
  ICON_CALENDAR,
} from '../../assets/images';
import Row from '../../components/atoms/row';
import InputBox from '../../components/input-box';
import colors from '../../config/colors';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import IconComponent from '../../components/icon-component';
import PickerInput from '../../components/picker-input';
import DateTimePicker from '@react-native-community/datetimepicker';
import {BloodGroupPicker} from '../onboarding/health-data';
import BottomSheet, {
  BottomSheetView,
  BottomSheetBackdrop,
} from '@gorhom/bottom-sheet';
import defaultBottomSheetConfigs from '../../utils/bottom-sheet-config';
import {Easing} from 'react-native-reanimated';
import moment from 'moment';
import RadioPicker from '../../components/radio-picker';
import ChipsRow from '../../components/chips-row';
import CameraPickerOptions from '../../components/camera-picker-options';
import {useDispatch, useSelector} from 'react-redux';
import idx from 'idx';
import {createPatient, uploadImage} from './edit-profile.actions';
import ImageResizer from 'react-native-image-resizer';
import SimpleToast from 'react-native-simple-toast';

const RELATION_SHIP = [
  {id: 1, value: 'SELF'},
  {id: 2, value: 'WIFE'},
  {id: 3, value: 'MOTHER'},
  {id: 4, value: 'FATHER'},
  {id: 5, value: 'SON'},
  {id: 6, value: 'DAUGHTER'},
];

const GENDERS = [
  {id: 1, value: 'MALE'},
  {id: 2, value: 'FEMALE'},
  {id: 3, value: 'OTHER'},
];

const PHOTO = 'PHOTO';
const VIDEO = 'VIDEO';
const CLOUD = 'CLOUD';
const GALLERY = 'GALLERY';

const PICKER_OPTIONS = [
  {
    text: 'Take a photo',
    id: PHOTO,
  },
  {
    text: 'Take a video',
    id: VIDEO,
  },
  {
    text: 'Selct from Docs App Library',
    id: CLOUD,
  },
  {
    text: 'Select From Photos',
    id: GALLERY,
  },
];

const Title = ({text}) => <Text style={styles.title}>{text}</Text>;

const TitleDescription = ({text}) => (
  <Text style={styles.subTitle}>{text}</Text>
);

export default function EditProfile({navigation}) {
  const datePlaceHolder = new Date();
  const [firstName, setFirstName] = useState(null);
  const [lastName, setLastName] = useState(null);
  const [phoneNumber, setPhoneNumber] = useState(null);
  const [relation, setrelation] = useState(null);
  const [gender, setGender] = useState(null);
  const [bloodGroup, setBloodGroup] = useState(null);
  const [uploadedImage, setUploadedImage] = useState(null);

  const [birthDay, setBirthDay] = useState(datePlaceHolder);
  const birthdaySheet = createRef();
  const bloodGroupSheet = createRef();
  const relationSheet = createRef();
  const attachmentOptionsSheet = createRef();
  const genderSheet = createRef();

  const config = useSelector((state) => state.appStateReducer.config);
  const userId =
    useSelector((state) => idx(state, (_) => _.user.userData.id)) || 1;

  const dispatch = useDispatch();

  useEffect(() => {
    if ((relation || {}).id) {
      relationSheet.current.snapTo(0);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [relation]);

  useEffect(() => {
    if ((gender || {}).id) {
      genderSheet.current.snapTo(0);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [gender]);

  useEffect(() => {
    if (bloodGroup) {
      bloodGroupSheet.current.snapTo(0);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [bloodGroup]);

  useEffect(() => {
    if (uploadedImage) {
      attachmentOptionsSheet.current.snapTo(0);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [uploadedImage]);

  const uploadImageAfterResizing = (image = {}, next, type) => {
    try {
      const path = type === 'RECENT_GALLERY' ? image.uri : image.path;
      ImageResizer.createResizedImage(
        path,
        960,
        (image.height / image.width) * 960,
        'JPEG',
        100,
        0,
        null,
      )
        .then((response) => {
          const imageToBeSent = {...response, type: 'image/jpg'};
          const newLocalPath = {
            ...image,
          };
          if (!newLocalPath.filename) {
            newLocalPath.filename = imageToBeSent.name;
          }
          next({
            image: imageToBeSent,
            localPath: newLocalPath,
            type: 'GALLERY',
          });
        })
        .catch((error) => {
          next({error});
        });
    } catch (error) {
      next({error});
    }
  };

  const uploadImageRequest = async ({image}) => {
    var photo = {
      uri: image.uri,
      type: 'image/jpeg',
      name: 'photo.jpg',
    };
    const payload = new FormData();
    payload.append('upload', photo);
    let {response, error} = await dispatch(
      uploadImage(config, payload, userId),
    );
    if (error) {
      SimpleToast.show('Failed to upload the image , please try again');
      return;
    }
    let url = idx(response, (_) => _.url);
    setUploadedImage(url);
  };

  const onImageSelected = (image) => {
    const type = 'CAMERA';
    uploadImageAfterResizing(image, uploadImageRequest, type);
  };

  const savePatientDataRequest = async () => {
    let payload = {
      userId: userId,
      name: firstName,
      surname: lastName,
      phone: phoneNumber,
      image: uploadedImage,
      address: 'test address',
      gender: (gender || {}).value,
      dob: birthDay,
      bloodGroup: (bloodGroup || {}).text,
      alergies: 'String',
      previousCondition: 'String',
      parentRelation: (relation || {}).value,
    };
    let {error} = await dispatch(createPatient(config, payload));
    if (error) {
      SimpleToast.show('Failed to add profile , please try again');
      return;
    }
    navigation.goBack();
  };

  return (
    <View style={styles.wrapper}>
      <Header
        title="Add Profile"
        RightComponent={() => (
          <TouchableOpacity onPress={savePatientDataRequest}>
            <Text style={styles.saveButton}>Save</Text>
          </TouchableOpacity>
        )}
      />
      <ScrollView contentContainerStyle={styles.scrollView}>
        <View style={styles.contentWrapper}>
          <Row>
            <TouchableOpacity
              onPress={() => attachmentOptionsSheet.current.snapTo(1)}>
              {uploadedImage ? (
                <Image
                  source={{uri: uploadedImage}}
                  style={styles.avatarImage}
                />
              ) : (
                <Image
                  source={DOCTOR_IMAGE_PLACEHOLDER}
                  style={styles.avatarImage}
                />
              )}

              <IconComponent
                icon={ICON_EDIT}
                size={18}
                color={colors.white}
                outerStyle={styles.imageEditButton}
                onPress={() => attachmentOptionsSheet.current.snapTo(1)}
              />
            </TouchableOpacity>

            <View style={styles.avatarDataWrapper}>
              <Text style={styles.userName}>James Franco</Text>
              <Text style={styles.relation}>Set Relation</Text>
            </View>
          </Row>
          <View style={styles.formWrapper}>
            <Title text="Personal Information" />
            <Row customStyles={styles.formSplitWrapper}>
              <View style={styles.formSplitLeft}>
                <InputBox
                  placeholder="First Name"
                  value={firstName}
                  onChangeText={(name) => setFirstName(name)}
                />
              </View>
              <View style={styles.formSplitRight}>
                <InputBox
                  placeholder="Surname"
                  value={lastName}
                  onChangeText={(name) => setLastName(name)}
                />
              </View>
            </Row>
            <InputBox
              placeholder="Phone Number"
              value={phoneNumber}
              onChangeText={(num) => setPhoneNumber(num)}
              keyboardType="phone-pad"
              maxLength={10}
            />
            <PickerInput
              placeholder="Date of birth"
              icon={ICON_CALENDAR}
              onPress={() => birthdaySheet.current.snapTo(1)}
              value={moment(birthDay).format('DD MMM YYYY')}
            />
            <PickerInput
              placeholder="Geneder"
              icon={ICON_DROPDOWN}
              onPress={() => genderSheet.current.snapTo(1)}
              value={(gender || {}).value}
            />
            <PickerInput
              placeholder="Relationship"
              icon={ICON_DROPDOWN}
              onPress={() => relationSheet.current.snapTo(1)}
              value={(relation || {}).value}
            />
            <PickerInput
              placeholder="Blood Group"
              icon={ICON_DROPDOWN}
              onPress={() => bloodGroupSheet.current.snapTo(1)}
              value={(bloodGroup || {}).text}
              disableTitlecase
            />
          </View>
          <Row>
            <View
              style={{
                flex: 1,
              }}>
              <Title text="Pre Conditions" />
              <TitleDescription text="Add your pre condtions here" />
            </View>
            <IconComponent icon={ICON_EDIT} size={20} color={colors.azul} />
          </Row>

          <ChipsRow count={5} perRowCount={3} />

          <Row>
            <View
              style={{
                flex: 1,
              }}>
              <Title text="Allergies" />
              <TitleDescription text="Add your allergies here" />
            </View>
            <IconComponent icon={ICON_EDIT} size={20} color={colors.azul} />
          </Row>

          <ChipsRow count={16} perRowCount={3} />

          {/* <MedicalIssuesPicker onSelect={() => null} />
          <Title text="Existing Conditons" /> */}
        </View>
      </ScrollView>

      <BottomSheet
        ref={birthdaySheet}
        snapPoints={[-50, heightPercentageToDP(40)]}
        initialSnapIndex={-1}
        animationDuration={defaultBottomSheetConfigs.animationDuration}
        animationEasing={Easing.out(Easing.back(1))}
        backdropComponent={BottomSheetBackdrop}
        onChange={(index) => {}}>
        <BottomSheetView>
          <View style={styles.datePickerWrapper}>
            {birthDay ? (
              <DateTimePicker
                testID="dateTimePicker"
                value={birthDay}
                mode="date"
                is24Hour={true}
                display="default"
                onChange={(_, date) => {
                  setBirthDay(date);
                }}
              />
            ) : null}
          </View>
        </BottomSheetView>
      </BottomSheet>

      <BottomSheet
        ref={bloodGroupSheet}
        snapPoints={[-50, heightPercentageToDP(20)]}
        initialSnapIndex={-1}
        animationDuration={defaultBottomSheetConfigs.animationDuration}
        animationEasing={Easing.out(Easing.back(1))}
        backdropComponent={BottomSheetBackdrop}
        onChange={(index) => {}}>
        <BottomSheetView>
          <View style={styles.bloodGroupPickerWrapper}>
            <BloodGroupPicker
              onSelect={(group) => setBloodGroup(group)}
              title=""
            />
          </View>
        </BottomSheetView>
      </BottomSheet>

      <BottomSheet
        ref={relationSheet}
        snapPoints={[-50, heightPercentageToDP(40)]}
        initialSnapIndex={-1}
        animationDuration={defaultBottomSheetConfigs.animationDuration}
        animationEasing={Easing.out(Easing.back(1))}
        backdropComponent={BottomSheetBackdrop}
        onChange={(index) => {}}>
        <BottomSheetView>
          <RadioPicker
            onPress={(item) => setrelation(item)}
            selectedId={(relation || {}).id}
            items={RELATION_SHIP}
          />
        </BottomSheetView>
      </BottomSheet>

      <BottomSheet
        ref={genderSheet}
        snapPoints={[-50, heightPercentageToDP(40)]}
        initialSnapIndex={-1}
        animationDuration={defaultBottomSheetConfigs.animationDuration}
        animationEasing={Easing.out(Easing.back(1))}
        backdropComponent={BottomSheetBackdrop}
        onChange={(index) => {}}>
        <BottomSheetView>
          <RadioPicker
            onPress={(item) => setGender(item)}
            selectedId={(gender || {}).id}
            items={GENDERS}
          />
        </BottomSheetView>
      </BottomSheet>

      <BottomSheet
        ref={attachmentOptionsSheet}
        snapPoints={[-50, heightPercentageToDP(75)]}
        initialSnapIndex={-1}
        animationDuration={defaultBottomSheetConfigs.animationDuration}
        animationEasing={Easing.out(Easing.back(1))}
        backdropComponent={BottomSheetBackdrop}>
        <BottomSheetView>
          <View style={styles.bsContentWrapper}>
            <CameraPickerOptions
              options={PICKER_OPTIONS}
              onSelectMedia={(media) => onImageSelected(media)}
            />
          </View>
        </BottomSheetView>
      </BottomSheet>
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 16,
    fontWeight: '500',
    fontStyle: 'normal',
    lineHeight: 24,
    letterSpacing: 0,
    color: '#00133d',
  },
  subTitle: {
    fontSize: 12,
    fontWeight: '300',
    fontStyle: 'normal',
    lineHeight: 24,
    letterSpacing: 0,
    color: '#00133d',
  },
  wrapper: {
    flex: 1,
  },
  contentWrapper: {
    paddingHorizontal: 10,
    paddingVertical: 20,
    backgroundColor: colors.whiteTwo,
    flex: 1,
  },
  avatarImage: {
    width: 56,
    height: 56,
    borderRadius: 56 / 2,
  },
  imageEditButton: {
    position: 'absolute',
    right: 0,
    bottom: 0,
    width: 20,
    height: 20,
    borderRadius: 20 / 2,
    backgroundColor: '#003cbf',
  },
  avatarDataWrapper: {
    flex: 1,
    paddingLeft: 20,
    height: 60,
    justifyContent: 'space-around',
  },
  userName: {
    fontSize: 16,
    fontWeight: '500',
    fontStyle: 'normal',
    lineHeight: 24,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#00133d',
  },
  relation: {
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 24,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#95a0b8',
  },
  formWrapper: {
    marginVertical: 20,
    height: heightPercentageToDP('50%'),
    justifyContent: 'space-around',
  },
  formSplitLeft: {
    flex: 1,
    marginRight: 3,
  },
  formSplitRight: {
    flex: 1,
    marginRight: 3,
  },
  formSplitWrapper: {
    marginVertical: 10,
  },
  scrollView: {
    // flex: 1,
  },
  saveButton: {
    fontSize: 18,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#4a5cd0',
  },
  bloodGroupPickerWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    height: heightPercentageToDP(20),
    width: widthPercentageToDP(100),
    padding: widthPercentageToDP(5),
  },
});
