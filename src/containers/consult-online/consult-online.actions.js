import {createAction} from 'redux-actions';
import {get, post} from 'api';
import {
  SEARCH_SYMPTOMS_REQUEST,
  SEARCH_SYMPTOMS_SUCCESS,
  SEARCH_SYMPTOMS_FAILED,
  INIT_CASE_REQUEST,
  INIT_CASE_SUCCESS,
  INIT_CASE_FAILED,
  SUBMIT_CASE_REQUEST,
  SUBMIT_CASE_SUCCESS,
  SUBMIT_CASE_FAILED,
  GET_CARE_TEAMS_REQUEST,
  GET_CARE_TEAMS_SUCCESS,
  GET_CARE_TEAMS_FAILED,
} from './consult-online.actions.const';
import idx from 'idx';
import {MEDICAL_CORE, USER_SERVICE} from '../../redux/api/service.names';

const searchSymptomsRequest = createAction(SEARCH_SYMPTOMS_REQUEST);
const searchSymptomsSuccess = createAction(SEARCH_SYMPTOMS_SUCCESS);
const searchSymptomsFailed = createAction(SEARCH_SYMPTOMS_FAILED);

export function searchSymptoms(config, search) {
  return async (dispatch) => {
    dispatch(searchSymptomsRequest());
    let response;
    let error;

    try {
      const res = await get(
        `${config.baseURL}`,
        `${`/${MEDICAL_CORE}/medicalcore/symptoms?search=${search}`}`,
      );
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(searchSymptomsSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(searchSymptomsFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(searchSymptomsFailed(error));
    }

    return {response, error};
  };
}

const addSymptomToCaseRequest = createAction(SEARCH_SYMPTOMS_REQUEST);
const addSymptomToCaseSuccess = createAction(SEARCH_SYMPTOMS_SUCCESS);
const addSymptomToCaseFailed = createAction(SEARCH_SYMPTOMS_FAILED);

export function addSymptomToCase(config, payload) {
  return async (dispatch) => {
    dispatch(addSymptomToCaseRequest());
    let response;
    let error;

    try {
      const res = await post(
        `${config.baseURL}`,
        `${`/${MEDICAL_CORE}/symptom-occurance/add-symptom-occurance`}`,
        payload,
      );
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(addSymptomToCaseSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(addSymptomToCaseFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(addSymptomToCaseFailed(error));
    }

    return {response, error};
  };
}

const initCaseRequest = createAction(INIT_CASE_REQUEST);
const initCaseSuccess = createAction(INIT_CASE_SUCCESS);
const initCaseFailed = createAction(INIT_CASE_FAILED);

export function initCase(config, payload) {
  return async (dispatch) => {
    dispatch(initCaseRequest());
    let response;
    let error;

    try {
      const res = await post(
        `${config.baseURL}`,
        `${`/${MEDICAL_CORE}/case/initCase/`}`,
        payload,
      );
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(initCaseSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(initCaseFailed(error));
      }
    } catch (ex) {
      console.log(ex);
      error = ex;
      dispatch(initCaseFailed(error));
    }

    return {response, error};
  };
}

const submitCaseRequest = createAction(SUBMIT_CASE_REQUEST);
const submitCaseSuccess = createAction(SUBMIT_CASE_SUCCESS);
const submitCaseFailed = createAction(SUBMIT_CASE_FAILED);

export function submitCase(config, payload) {
  return async (dispatch) => {
    dispatch(submitCaseRequest());
    let response;
    let error;

    try {
      const res = await post(
        `${config.baseURL}`,
        `${`/${MEDICAL_CORE}/case/submit`}`,
        payload,
      );
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(submitCaseSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(submitCaseFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(submitCaseFailed(error));
    }

    return {response, error};
  };
}

const getCareTeamsRequest = createAction(GET_CARE_TEAMS_REQUEST);
const getCareTeamsSuccess = createAction(GET_CARE_TEAMS_SUCCESS);
const getCareTeamsFailed = createAction(GET_CARE_TEAMS_FAILED);

export function getCareTeams(config) {
  return async (dispatch) => {
    dispatch(getCareTeamsRequest());
    let response;
    let error;

    try {
      const res = await get(
        `${config.baseURL}`,
        `${`/${USER_SERVICE}/care-team/`}`,
      );
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(getCareTeamsSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(getCareTeamsFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(getCareTeamsFailed(error));
    }

    return {response, error};
  };
}
