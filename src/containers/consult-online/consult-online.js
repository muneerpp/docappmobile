/* eslint-disable react-hooks/exhaustive-deps */
import React, {createRef, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Dimensions,
  TouchableOpacity,
  TextInput,
  Keyboard,
  Image,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {
  ACCOUNT,
  ADD_ICON,
  HELP,
  SEARCH_ICON_SECOND,
  SYMPTOM_PLACE_HOLDER,
  ICON_CLOSE,
} from 'images';
import IconComponent from '../../components/icon-component';

const PHOTO = 'PHOTO';
const VIDEO = 'VIDEO';
const CLOUD = 'CLOUD';
const GALLERY = 'GALLERY';

const PICKER_OPTIONS = [
  {
    text: 'Take a photo',
    id: PHOTO,
  },
  {
    text: 'Take a video',
    id: VIDEO,
  },
  {
    text: 'Selct from Docs App Library',
    id: CLOUD,
  },
  {
    text: 'Select From Photos',
    id: GALLERY,
  },
];

import HeaderNavigation from '../../components/molecules/header-navigation';
import colors from '../../config/colors';
// import BottomSheet from 'reanimated-bottom-sheet';
import {isIphoneX} from 'react-native-iphone-x-helper';
import BottomSheetHeader from '../../components/bottom-sheet-header';
import SuggestedSymptoms from '../../components/molecules/suggested-symptoms';
import CardWrapper from '../../components/molecules/card-wrapper';
import SymptomChip from '../../components/symptoms-chip';
import UserPickerConsult from '../../components/user-picker-consult';
import TextInputMedium from '../../components/text-input-medium';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {useEffect} from 'react';
import CameraPickerOptions from '../../components/camera-picker-options';
import SimpleToast from 'react-native-simple-toast';
import idx from 'idx';
import {titleCase} from '../../utils/helpers';
import CareTeamPicker from '../../components/care-team-picker';
import {chain} from 'icepick';

import BottomSheet, {
  BottomSheetView,
  BottomSheetBackdrop,
} from '@gorhom/bottom-sheet';
import defaultBottomSheetConfigs from '../../utils/bottom-sheet-config';
import {Easing} from 'react-native-reanimated';
import {heightPercentageToDP} from 'react-native-responsive-screen';

const SearchBox = ({onChangeText, onCancel}) => (
  <View style={styles.searchWrapper}>
    <View style={styles.searchBox}>
      <IconComponent icon={SEARCH_ICON_SECOND} size={20} />
      <TextInput
        style={styles.input}
        placeholder="Eg : Fever"
        onChangeText={onChangeText}
      />
    </View>
    <TouchableOpacity onPress={onCancel}>
      <Text style={styles.cancel}>Cancel</Text>
    </TouchableOpacity>
  </View>
);

const BottomButton = ({isActive, onPress}) => (
  <View style={styles.bottomButtonWrapper} onPress={onPress}>
    <Text style={styles.bottomButtonTextInfo}>
      You details will remain 100% private and secure
    </Text>
    <TouchableOpacity onPress={onPress}>
      <LinearGradient
        style={styles.bottonButton}
        colors={
          isActive
            ? [colors.topaz, colors.brightCyan]
            : ['rgba(1,1,1,0.2)', 'rgba(1,1,1,0.7)']
        }>
        <Text
          style={[
            styles.bottomButtonText,
            {
              color: isActive ? colors.white : colors.paleGrey,
            },
          ]}>
          Continue
        </Text>
      </LinearGradient>
    </TouchableOpacity>
  </View>
);

const CaseAttachment = () => (
  <View style={styles.caseAttchmentWrapper}>
    <Image source={SYMPTOM_PLACE_HOLDER} style={styles.caseAttchmentImage} />
    <View style={styles.caseAttchmentContentWrapper}>
      <Text style={styles.caseAttchmentContentText}>
        Redness on the back of the neck
      </Text>
      <Text style={styles.caseAttchmentUploadedOn}>Uploaded Jan, 05 2020</Text>
    </View>
    <IconComponent icon={ICON_CLOSE} size={16} />
  </View>
);

export default function ConsultOnline({
  profiles,
  config,
  searchSymptoms,
  initCase,
  activeCase,
  addSymptomToCase,
  userData,
  submitCase,
  getCareTeams,
  careTeams,
  navigation,
}) {
  const addSymptomSheet = createRef();
  const scrollView = createRef();
  const inputRef = createRef();
  const attachmentOptionsSheet = createRef();
  const careTeamPickerSheet = createRef();
  const [isSymptomOpen, setSymptomOpen] = useState(false);
  const [symptoms, setSymptoms] = useState([]);
  const [activeProfile, setActiveProfile] = useState(profiles[0]);

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', _keyboardDidShow);

    // cleanup function
    return () => {
      Keyboard.removeListener('keyboardDidShow', _keyboardDidShow);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const _keyboardDidShow = () => {
    // scrollToItem();
  };

  const initCaseRequest = async () => {
    let payload = {
      ownerProfileId: activeProfile.id,
    };
    await initCase(config, payload);
  };

  const getCareTeamsRequest = async () => {
    let {response, error} = await getCareTeams(config);
  };

  useEffect(() => {
    initCaseRequest();
    getCareTeamsRequest();
  }, []);

  useEffect(() => {
    initCaseRequest();
  }, [activeProfile]);

  const scrollToItem = () => {
    requestAnimationFrame(() => {
      inputRef.current.measure((width, height, px, py, fx, fy) => {
        scrollView.current.scrollTo({x: 0, y: height, animated: true});

        // this.setState({scrollToHeight: fy});
        // this.scrollToView(fy - 70);
      });
    });
  };

  const handleAddAttachment = () => {
    attachmentOptionsSheet.current.snapTo(1);
  };

  const getSymptoms = async (search = 'cell') => {
    let text = String(search).toLowerCase();
    if (text.length > 3) {
      let {response, error} = await searchSymptoms(config, text);
      if (response) {
        setSymptoms(response);
      }
    }
  };

  const addSymptomToCaseRequest = async (symptom) => {
    let payload = {
      userId: userData.id,
      addedByUserId: userData.id,
      caseId: activeCase.id,
      symptomId: symptom.id,
      occuranceValue: {
        name: 'Temperature',
        value: {
          temp: 102,
          unit: 'F',
        },
        id: 1,
      },
      notes: 'Test Note Created Now',
    };
    let {error} = await addSymptomToCase(config, payload);
    if (error) {
      SimpleToast.show('Failed to add symptom the case , please try again');
      return;
    }
    await initCaseRequest();
    SimpleToast.show('Added symptom successfully to the case');
  };

  const submitCaseRequest = async (careTeam) => {
    let payload = chain(activeCase).setIn(['careTeamId'], careTeam.id).value();
    let {error} = await submitCase(config, payload);
    if (error) {
      SimpleToast.show(
        'Failed to submit the case, please trya again',
        SimpleToast.BOTTOM,
      );
    }
    navigation.goBack();
  };

  const pickCareTeam = () => {
    careTeamPickerSheet.current.snapTo(1);
  };

  return (
    <View style={styles.screen}>
      <HeaderNavigation title="Consult Online" />

      <ScrollView ref={scrollView}>
        <View style={styles.cardWrapper}>
          <CardWrapper title="Is this for You or Someone else?" icon={ACCOUNT}>
            <UserPickerConsult
              profiles={profiles}
              onPress={(pat) => {
                setActiveProfile(pat);
              }}
              activeProfile={activeProfile}
              navigation={navigation}
            />
          </CardWrapper>
        </View>
        <View style={styles.cardWrapper}>
          <CardWrapper title="Select your symptom or condition" icon={HELP}>
            <View style={styles.symptomsWrapper}>
              {activeCase &&
                activeCase.symptomOccurances.map((symptom) => (
                  <View style={styles.symptomWrapper}>
                    <Text style={styles.symptomTitle}>
                      {titleCase(idx(symptom, (_) => _.symptom.name) || '')}
                    </Text>
                    <Text style={styles.symptomStartedAgo}>
                      {titleCase(idx(symptom, (_) => _.symptom.name) || '')}
                    </Text>
                  </View>
                ))}
            </View>
            <TouchableOpacity
              style={styles.addConditionWrapper}
              onPress={() => addSymptomSheet.current.snapTo(1)}>
              <IconComponent icon={ADD_ICON} size={18} />
              <Text style={styles.addConditionText}>
                Add Condition or symptom
              </Text>
            </TouchableOpacity>
          </CardWrapper>
        </View>
        <View style={styles.cardWrapper}>
          <CardWrapper title="Additional Information" icon={ACCOUNT}>
            <View style={{paddingHorizontal: 24, paddingTop: 15}}>
              <View ref={inputRef} />
              <TextInputMedium />
              <CaseAttachment />
              <CaseAttachment />
              <CaseAttachment />
              <CaseAttachment />
              <TouchableOpacity
                style={styles.addAttachmentWrapper}
                onPress={handleAddAttachment}>
                <IconComponent icon={ADD_ICON} size={18} />
                <Text style={styles.addConditionText}>Add Attachment</Text>
              </TouchableOpacity>
            </View>
          </CardWrapper>
        </View>
        <KeyboardSpacer />
      </ScrollView>
      <BottomButton
        isActive={activeCase && activeCase.symptomOccurances.length > 0}
        onPress={pickCareTeam}
      />

      <BottomSheet
        ref={addSymptomSheet}
        snapPoints={[-50, heightPercentageToDP(75)]}
        initialSnapIndex={-1}
        animationDuration={defaultBottomSheetConfigs.animationDuration}
        animationEasing={Easing.out(Easing.back(1))}
        backdropComponent={BottomSheetBackdrop}
        onChange={(index) => {
          if (index === 0) {
            Keyboard.dismiss();
          }
        }}>
        <BottomSheetView>
          <View style={styles.bsContentWrapper}>
            <SearchBox
              onChangeText={(text) => getSymptoms(text)}
              onCancel={() => addSymptomSheet.current.snapTo(0)}
            />

            {symptoms.length > 0 ? (
              <View>
                {symptoms.map((symptom) => (
                  <SymptomChip
                    onPress={() => {
                      addSymptomToCaseRequest(symptom);
                      addSymptomSheet.current.snapTo(0);
                      Keyboard.dismiss();
                    }}
                    {...symptom}
                    isSelected={true}
                    width={'100%'}
                  />
                ))}
              </View>
            ) : (
              <SuggestedSymptoms />
            )}
          </View>
        </BottomSheetView>
      </BottomSheet>

      <BottomSheet
        ref={attachmentOptionsSheet}
        snapPoints={[-50, heightPercentageToDP(75)]}
        initialSnapIndex={-1}
        animationDuration={defaultBottomSheetConfigs.animationDuration}
        animationEasing={Easing.out(Easing.back(1))}
        backdropComponent={BottomSheetBackdrop}
        onChange={(index) => {
          if (index === 0) {
            Keyboard.dismiss();
          }
        }}>
        <BottomSheetView>
          <View style={styles.bsContentWrapper}>
            <CameraPickerOptions
              options={PICKER_OPTIONS}
              onSelectMedia={(media) => null}
            />
          </View>
        </BottomSheetView>
      </BottomSheet>

      <BottomSheet
        ref={careTeamPickerSheet}
        snapPoints={[-50, heightPercentageToDP(75)]}
        initialSnapIndex={-1}
        animationDuration={defaultBottomSheetConfigs.animationDuration}
        animationEasing={Easing.out(Easing.back(1))}
        backdropComponent={BottomSheetBackdrop}
        onChange={(index) => {
          if (index === 0) {
            Keyboard.dismiss();
          }
        }}>
        <BottomSheetView>
          <CareTeamPicker
            careTeams={careTeams}
            onSelectCareTeam={(careTeam) => {
              submitCaseRequest(careTeam);
              careTeamPickerSheet.current.snapTo(0);
            }}
          />
        </BottomSheetView>
      </BottomSheet>
    </View>
  );
}

const styles = StyleSheet.create({
  cardWrapper: {
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  bottomButtonWrapper: {
    height: 124,
    backgroundColor: 'rgba(255, 255, 255, 0.68)',
  },
  bottomButtonTextInfo: {
    fontSize: 11,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 14,
    letterSpacing: 0,
    textAlign: 'center',
    color: '#1e1f20',
    marginVertical: 10,
  },
  bottonButton: {
    height: 50,
    borderRadius: 12,
    marginHorizontal: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomButtonText: {
    fontSize: 15,
    fontWeight: 'bold',
    fontStyle: 'normal',
    lineHeight: 18,
    letterSpacing: 0,
    textAlign: 'left',
  },
  symptomWrapper: {
    height: 40,
    borderRadius: 4,
    backgroundColor: '#f6f6f6',
    marginBottom: 20,
    alignItems: 'center',
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  symptomTitle: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
  },
  symptomStartedAgo: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#9393aa',
  },
  symptomsWrapper: {
    paddingHorizontal: 24,
    marginTop: 10,
  },
  screen: {
    backgroundColor: colors.screenBg,
    flex: 1,
  },
  addConditionWrapper: {
    height: 36,
    borderRadius: 8,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#e0e0e0',
    marginHorizontal: 26,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  addAttachmentWrapper: {
    height: 36,
    borderRadius: 8,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: '#e0e0e0',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: 10,
  },
  addConditionText: {
    fontSize: 13,
    fontWeight: '500',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#9393aa',
    marginLeft: 10,
  },
  searchWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10,
    marginTop: 10,
  },
  searchBox: {
    flexDirection: 'row',
    height: 48,
    borderRadius: 12,
    backgroundColor: colors.white,
    shadowColor: 'rgba(147, 147, 170, 0.12)',
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowRadius: 10,
    shadowOpacity: 1,
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: colors.topaz,
    alignItems: 'center',
    paddingHorizontal: 10,
    flex: 1,
    marginRight: 10,
  },
  input: {
    flex: 1,
    marginHorizontal: 10,
  },
  cancel: {
    fontSize: 15,
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'right',
    color: '#1da1f2',
  },
  bsContentWrapper: {
    backgroundColor: colors.white,
    height: Dimensions.get('window').height,
  },
  caseAttchmentWrapper: {
    minHeight: 80,
    borderRadius: 4,
    backgroundColor: '#f6f6f6',
    padding: 5,
    marginVertical: 5,
    flexDirection: 'row',
  },
  caseAttchmentImage: {
    width: 100,
    height: 72,
    borderRadius: 4,
  },
  caseAttchmentContentWrapper: {
    flex: 1,
    paddingHorizontal: 10,
    justifyContent: 'space-around',
  },
  caseAttchmentContentText: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: '#1e1f20',
  },
  caseAttchmentUploadedOn: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    lineHeight: 16,
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blueyGrey,
  },
});
