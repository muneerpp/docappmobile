import {connect} from 'react-redux';
import ConsultOnline from './consult-online';
import {
  searchSymptoms,
  initCase,
  addSymptomToCase,
  submitCase,
  getCareTeams,
} from './consult-online.actions';
import idx from 'idx';

const mapStateToProps = (state) => {
  let config = idx(state, (_) => _.appStateReducer.config) || {};
  const userData = idx(state, (_) => _.user.userData);
  const profiles = idx(state, (_) => _.user.profiles) || [];
  const activeCase = idx(state, (_) => _.consultation.activeCase);
  const careTeams = idx(state, (_) => _.consultation.careTeams);
  return {
    config,
    profiles,
    activeCase,
    userData,
    submitCase,
    careTeams,
  };
};

export default connect(mapStateToProps, {
  searchSymptoms,
  initCase,
  addSymptomToCase,
  submitCase,
  getCareTeams,
})(ConsultOnline);
