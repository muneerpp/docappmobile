import {chain} from 'icepick';
import {GET_CASE_DETAILS_SUCCESS} from '../case-details/case-details.actions.const';
import {GET_CASES_LIST_SUCCESS} from '../home-screen/home-screen.actions.const';

const initialState = {
  cases: [],
  activeCase: null,
  careTeams: [],
};

export default (state = initialState, {type, payload}) => {
  switch (type) {
    case 'INIT_CASE_SUCCESS':
    case GET_CASE_DETAILS_SUCCESS:
      return chain(state).assocIn(['activeCase'], payload).value();

    case 'GET_CARE_TEAMS_SUCCESS':
      return chain(state).assocIn(['careTeams'], payload).value();

    case GET_CASES_LIST_SUCCESS:
      return chain(state).assocIn(['cases'], payload).value();

    default:
      return state;
  }
};
