import {createAction} from 'redux-actions';
import {LOGOUT_USER} from './account.actions.const';

const logoutRequest = createAction(LOGOUT_USER);

export const logout = () => (dispatch) => dispatch(logoutRequest());
