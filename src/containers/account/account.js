import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {useDispatch, useSelector} from 'react-redux';
import {
  DOCTOR_IMAGE_PLACEHOLDER,
  LOCATION_PIN,
  CREDIT_CARD,
  ICON_MEMBERSHIP,
  CUSTOMER_SUPPORT,
  ICON_SETTINGS,
  ICON_LOGOUT,
  ICON_REPORT,
} from '../../assets/images';
import Row from '../../components/atoms/row';
import Title from '../../components/atoms/title';
import IconComponent from '../../components/icon-component';
import ScreenWrapper from '../../components/screen-wrapper';
import colors from '../../config/colors';
import {logout} from '../account/account.actions';

const PROFILE_ITEMS = [
  {
    icon: CREDIT_CARD,
    text: 'Platinum',
    rightText: '4500 INR',
    route: '',
  },
  {
    icon: ICON_REPORT,
    text: 'Medical Card',
    route: 'MedicalCard',
  },
  {
    icon: ICON_MEMBERSHIP,
    text: 'Membership Package',
  },
  {
    icon: CUSTOMER_SUPPORT,
    text: 'Help Center',
  },
  {
    icon: ICON_SETTINGS,
    text: 'Settings',
  },
  {
    icon: ICON_LOGOUT,
    text: 'Logout',
    id: 'LOGOUT',
  },
];

const AvatarImage = () => (
  <View>
    <View style={styles.avatarWrapper}>
      <Image source={DOCTOR_IMAGE_PLACEHOLDER} style={styles.avatar} />
    </View>
  </View>
);

export default function Account({navigation}) {
  const profiles = useSelector((state) => state.user.profiles) || [];
  const dispatch = useDispatch();
  let mainProfile = {};
  if (profiles.length > 0) {
    mainProfile = profiles.find((item) => item.parentRelation === 'SELF');
  }

  const onListItemPress = (item) => {
    if (item.route) {
      navigation.navigate(item.route);
      return;
    }

    if (item.id === 'LOGOUT') {
      dispatch(logout());
      setTimeout(() => {
        navigation.replace('SplashScreen');
      }, 100);
    }
  };
  return (
    <ScreenWrapper backgroundColor={colors.white}>
      <ScrollView contentContainerStyle={styles.accountWrapper}>
        <Title text="Profile" customStyle={styles.title} />
        <AvatarImage />
        <Title text={mainProfile.name} customStyle={styles.name} />

        <Row>
          <IconComponent icon={LOCATION_PIN} size={20} />
          <Text style={styles.address}>2250 G, Beach Road, New York, USA</Text>
        </Row>
        <View style={styles.itemWrapper}>
          {PROFILE_ITEMS.map((item) => (
            <TouchableOpacity onPress={() => onListItemPress(item)}>
              <Row customStyles={styles.itemRow}>
                <View style={styles.iconItemWrapper}>
                  <IconComponent icon={item.icon} size={20} />
                </View>
                <Row customStyles={styles.itemRowData}>
                  <Text style={styles.itemRowDataText}>{item.text}</Text>
                  {item.rightText ? (
                    <View>
                      <Text>{item.rightText}</Text>
                    </View>
                  ) : null}
                </Row>

                {/* <IconComponent /> */}
              </Row>
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>
    </ScreenWrapper>
  );
}

const styles = StyleSheet.create({
  avatarWrapper: {
    width: 95,
    height: 95,
    borderRadius: 95 / 2,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  avatar: {
    width: 90,
    height: 90,
    borderRadius: 90 / 2,
  },
  accountWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    marginBottom: heightPercentageToDP('2%'),
  },
  name: {
    fontSize: 14,
    paddingVertical: heightPercentageToDP('2%'),
  },
  address: {
    fontSize: 13,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#999999',
    marginLeft: 10,
  },
  itemWrapper: {
    width: widthPercentageToDP('85%'),
    marginVertical: heightPercentageToDP('10%'),
  },
  itemRow: {
    justifyContent: 'center',
    marginBottom: 20,
  },
  itemRowData: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: '#e8e8e8',
    justifyContent: 'center',
    marginLeft: heightPercentageToDP('3%'),
    paddingVertical: 10,
  },
  itemRowDataText: {
    fontSize: 15,
    fontWeight: '500',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#333333',
    flex: 1,
  },
  iconItemWrapper: {
    justifyContent: 'center',
  },
});
