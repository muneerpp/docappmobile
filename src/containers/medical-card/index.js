import React, {createRef, useState} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  ScrollView,
} from 'react-native';
import {
  DOCTOR_IMAGE_PLACEHOLDER,
  FEMALE_BODY,
  ICON_HEART,
  ICON_TEMPERATURE,
  ICON_OXYGEN,
} from '../../assets/images';
import Image from 'react-native-fast-image';

import HeaderNavigation from '../../components/molecules/header-navigation';
import Carousel from 'react-native-snap-carousel';

import colors from '../../config/colors';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import Title from '../../components/atoms/title';
import Row from '../../components/atoms/row';
import IconComponent from '../../components/icon-component';
import {useSelector} from 'react-redux';
import {titleCase} from '../../utils/helpers';
import moment from 'moment';

const PROFILES = [
  {
    id: 0,
    name: 'David',
  },
  {
    id: 0,
    name: 'David',
  },
  {
    id: 0,
    name: 'David',
  },
];

const ProfileItem = ({item, index, activeIndex, onPress}) => {
  return (
    <TouchableOpacity
      onPress={() => onPress(index)}
      style={styles.profileItemWrapper}>
      {item.image ? (
        <Image source={{uri: item.image}} style={styles.profileImage} />
      ) : (
        <Image source={DOCTOR_IMAGE_PLACEHOLDER} style={styles.profileImage} />
      )}
      <Title text={item.name} customStyle={styles.profileName} />
    </TouchableOpacity>
  );
};

const MetricItem = ({unit, value, type}) => (
  <View style={styles.metricItemWrapper}>
    <Text style={styles.metricItemUnit}>{unit}</Text>
    <Text style={styles.metricItemValue}>{value}</Text>
    <Text style={styles.metricItemType}>{type}</Text>
  </View>
);

const HealthMetricItem = ({value, icon, type}) => (
  <Row>
    <IconComponent icon={icon} size={30} />
    <View style={styles.healthDataWrapper}>
      <Text style={styles.healthDataValue}>{value}</Text>
      <Text style={styles.healthDataType}>{type}</Text>
    </View>
  </Row>
);

const HealthCard = () => (
  <View style={styles.healthCardWrapper}>
    <View style={styles.healthCardImageWrapper}>
      <Image source={FEMALE_BODY} style={styles.healthCardImage} />
    </View>
    <View style={styles.metricsWrapper}>
      <MetricItem unit="in" value={'5\' 3"'} type="Height" />
      <MetricItem unit="kg" value={'62"'} type="Weight" />
      <MetricItem unit="MBI" value={'20.4'} type="Average" />
    </View>
    <View>
      <View style={styles.healthMetricsDataWrapper}>
        <HealthMetricItem value="110" icon={ICON_HEART} type="Heartbeat" />
        <HealthMetricItem
          value="97"
          icon={ICON_OXYGEN}
          type="Oxygen Saturation"
        />
        <HealthMetricItem
          value="97.5F"
          icon={ICON_TEMPERATURE}
          type="Body Temperature"
        />
      </View>
    </View>
  </View>
);

const GeneralInformationItem = ({item, value}) => (
  <Row
    customStyles={{
      marginVertical: 10,
      paddingHorizontal: 10,
    }}>
    <Text>{titleCase(item)} : </Text>
    <Text>{titleCase(value)}</Text>
  </Row>
);

const GeneralInformation = ({
  gender,
  bloodGroup,
  previousCondition,
  dob,
  parentRelation,
}) => (
  <View
    style={{
      marginVertical: 40,
    }}>
    <GeneralInformationItem item="Gender" value={gender} />
    <GeneralInformationItem item="bloodGroup" value={bloodGroup} />
    <GeneralInformationItem
      item="previousCondition"
      value={previousCondition}
    />
    <GeneralInformationItem item="Age" value={moment(dob).fromNow(true)} />
    <GeneralInformationItem
      item="dob"
      value={moment(dob).format('DD-MMM-YYYY')}
    />
    <GeneralInformationItem item="Relation" value={parentRelation} />
  </View>
);

export default function MedicalCard() {
  const profileSwitcher = createRef();
  const [activeIndex, setActiveIndex] = useState(0);

  const profiles = useSelector((state) => state.user.profiles) || [];

  return (
    <View style={styles.container}>
      <HeaderNavigation title="Medical Card" />
      <View style={styles.screen}>
        <View style={styles.carousalWrapper}>
          <Carousel
            ref={profileSwitcher}
            data={profiles}
            renderItem={(props) => (
              <ProfileItem
                {...props}
                activeIndex={activeIndex}
                onPress={(index) => profileSwitcher.current.snapToItem(index)}
              />
            )}
            sliderWidth={widthPercentageToDP('100%')}
            itemWidth={widthPercentageToDP('35%')}
            onSnapToItem={(index) => setActiveIndex(index)}
          />
        </View>

        <ScrollView showsVerticalScrollIndicator={false}>
          <HealthCard {...profiles[activeIndex]} />
          <GeneralInformation {...profiles[activeIndex]} />
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  profileItemWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    paddingHorizontal: 15,
  },
  profileImage: {
    width: 120,
    height: 120,
    borderRadius: 32,
    marginBottom: 20,
  },
  profileName: {
    fontSize: 18,
  },
  metricItemWrapper: {
    height: heightPercentageToDP('6%'),
    justifyContent: 'space-between',
  },
  metricItemUnit: {
    fontSize: 12,
    fontWeight: '600',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
  },
  metricItemValue: {
    fontSize: 18,
    fontWeight: '600',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#ff8050',
  },
  metricItemType: {
    fontSize: 10,
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.black,
  },
  healthDataWrapper: {
    flex: 1,
    height: heightPercentageToDP('5%'),
    justifyContent: 'space-between',
    marginLeft: 10,
  },
  healthDataValue: {
    fontSize: 20,
    fontWeight: '600',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#4a5cd0',
  },
  healthDataType: {
    fontSize: 12,
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: '#4a5cd0',
    maxWidth: widthPercentageToDP('20%'),
    lineHeight: 16,
  },
  healthCardWrapper: {
    width: widthPercentageToDP('90%'),
    height: 250,
    backgroundColor: colors.white,
    shadowColor: colors.veryLightPink,
    borderRadius: 8,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowRadius: 10,
    shadowOpacity: 1,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  healthCardImageWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    width: widthPercentageToDP('30%'),
  },
  healthCardImage: {
    width: 67.6,
    height: 175,
    opacity: 0.5,
    marginRight: 10,
  },
  metricsWrapper: {
    flex: 1,
    justifyContent: 'space-between',
    paddingVertical: 18,
  },
  healthMetricsDataWrapper: {
    width: widthPercentageToDP('40%'),
    height: 250,
    backgroundColor: '#eaecf7',
    borderTopLeftRadius: 12,
    borderBottomLeftRadius: 12,
    justifyContent: 'space-around',
    paddingLeft: 20,
  },
  screen: {
    backgroundColor: colors.whiteTwo,
    flex: 1,
    paddingHorizontal: 10,
  },
  carousalWrapper: {
    width: widthPercentageToDP('100%'),
    alignSelf: 'center',
    paddingVertical: 20,
  },
  container: {
    flex: 1,
  },
});
