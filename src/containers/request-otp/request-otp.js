import React, {useState, useEffect} from 'react';
import {View, Image, Text, ScrollView, StyleSheet} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import SimpleToast from 'react-native-simple-toast';
import {APP_LOGO} from '../../assets/images';
import Button from '../../components/button';
import InputBox from '../../components/input-box';
import ScreenWrapper from '../../components/screen-wrapper';
import colors from '../../config/colors';

export default function RequestOTP({navigation, requestOTP, config}) {
  const [phoneNumber, setPhoneNumber] = useState(null);
  const [errorMessage, seterrorMessage] = useState('');

  const requestOTPRequest = async () => {
    let payload = {
      mobileNumber: phoneNumber,
      userType: 'END_USER',
    };
    let {error} = await requestOTP(config, payload);
    if (error) {
      SimpleToast.show('Something went wrong , please try again');
      return;
    }
    if (!error) {
      navigation.replace('VerifyOTP', {phoneNumber});
    }
  };

  useEffect(() => {
    if (!phoneNumber || phoneNumber.length < 10) {
      seterrorMessage('Please enter a valid phone number');
      return;
    } else {
      seterrorMessage('');
    }
  }, [phoneNumber]);

  return (
    <ScreenWrapper backgroundColor={colors.white}>
      <ScrollView
        contentContainerStyle={styles.scrollView}
        keyboardShouldPersistTaps="handled">
        <Image
          style={{
            width: heightPercentageToDP(8),
            height: heightPercentageToDP(8),
          }}
          source={APP_LOGO}
        />
        <Text style={styles.title}>Enter your phone number</Text>
        <Text style={styles.subTitle}>
          Please enter your phone number to receive a one time password
        </Text>
        <View style={styles.inputWrapper}>
          <InputBox
            prefix="+91"
            onChangeText={(number) => setPhoneNumber(number)}
            placeholder="Your Phone Number"
            value={phoneNumber}
            maxLength={10}
            autoFocus
          />
          {errorMessage ? (
            <Text style={styles.errorMessage}>{errorMessage}</Text>
          ) : null}
        </View>
        {!errorMessage ? (
          <Button
            label="Lets Go"
            color={errorMessage ? colors.blueyGrey : colors.azul}
            onPress={requestOTPRequest}
          />
        ) : (
          <Button
            label="Lets Go"
            color={errorMessage ? colors.blueyGrey : colors.azul}
          />
        )}
        <KeyboardSpacer />
      </ScrollView>
    </ScreenWrapper>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: widthPercentageToDP(5),
    paddingBottom: heightPercentageToDP(5),
  },
  title: {
    fontSize: 21,
    fontWeight: 'bold',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
    marginTop: heightPercentageToDP(3),
  },
  subTitle: {
    opacity: 0.35,
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.blackTwo,
    marginTop: heightPercentageToDP(1),
    width: widthPercentageToDP(80),
  },
  errorMessage: {
    fontSize: 16,
    fontWeight: 'normal',
    fontStyle: 'normal',
    letterSpacing: 0,
    textAlign: 'left',
    color: colors.brownishPink,
    marginTop: heightPercentageToDP(1),
    width: widthPercentageToDP(80),
  },
  inputWrapper: {
    flex: 1,
    marginTop: heightPercentageToDP(2),
  },
});
