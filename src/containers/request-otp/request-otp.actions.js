import {createAction} from 'redux-actions';
import {get, post} from 'api';
import idx from 'idx';
import {MEDICAL_CORE, USER_SERVICE} from '../../redux/api/service.names';
import {
  REQUEST_OTP_FAILURE,
  REQUEST_OTP_REQUEST,
  REQUEST_OTP_SUCCESS,
} from '../../redux/actions/user.actions.const';

const requestOTPRequest = createAction(REQUEST_OTP_REQUEST);
const requestOTPSuccess = createAction(REQUEST_OTP_SUCCESS);
const requestOTPFailed = createAction(REQUEST_OTP_FAILURE);

export function requestOTP(config, payload) {
  return async (dispatch) => {
    dispatch(requestOTPRequest());
    let response;
    let error;

    try {
      const res = await post(
        `${config.baseURL}`,
        `${`/${USER_SERVICE}/auth/requestOTP`}`,
        payload,
      );
      response = idx(res, (_) => _.response.data) || '';

      if (response) {
        dispatch(requestOTPSuccess(response));
      } else {
        // eslint-disable-next-line prefer-destructuring
        error = (res || {}).error;
        dispatch(requestOTPFailed(error));
      }
    } catch (ex) {
      error = ex;
      dispatch(requestOTPFailed(error));
    }

    return {response, error};
  };
}
