import {connect} from 'react-redux';
import RequestOTP from './request-otp';
import {requestOTP} from './request-otp.actions';
import idx from 'idx';

const mapStateToProps = (state) => {
  let config = idx(state, (_) => _.appStateReducer.config) || {};
  return {
    config,
  };
};

export default connect(mapStateToProps, {
  requestOTP,
})(RequestOTP);
